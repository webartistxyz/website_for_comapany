<?php include "include/single_page_header.php"; ?>
<?php
use App\Menu\Menu;
isset($_GET["pageId"]) ? $id = $_GET["pageId"] : header("Location: index.php");
$info = Menu::getMenuById($id);
if (!empty($info)){
?>

	<div class="top_panel_title_wrap">
		<div class="content_wrap">
			<div class="top_panel_title">
				<div class="page_title">
					<h3 class="page_caption"><?= $info['name'] ?></h3>
				</div>
				<div class="breadcrumbs">
					<a class="breadcrumbs_item home" href="index.php">Home</a>
					<span class="breadcrumbs_delimiter"></span>
					<span class="breadcrumbs_item current"><?= $info['name'] ?></span>
				</div>
			</div>
		</div>
	</div>
<br><br><br>
		<div class="page_content_wrap scheme_default">
			<div class="content_wrap">
				<div class="content">
					<div class="posts_container">
						<article class="post_item post_layout_excerpt post_format_video post format-video has-post-thumbnail hentry">
							<div class="post_content entry-content">
								<div class="post_content_inner">
									<p>
										<?php echo $info['link'] ?>
									</p>
								</div>
							</div>
						</article>

					</div>
					<div class="nav-links-old">
						<span class="nav-prev"></span>
						<span class="nav-next"></span>
					</div>

				</div>
				<?php include "include/right_sidebar.php" ;?>
			</div>
		</div>
	<?php } ?>
<?php include "include/footer.php"; ?>