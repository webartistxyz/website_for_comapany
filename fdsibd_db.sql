-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2017 at 12:10 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fdsibd_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `address`, `location`, `email`, `mobile`) VALUES
(1, '<h4 style="padding: 0px; margin: 0px; outline: 0px; clear: both; font-family: \'Open Sans\', sans-serif; color: #333333; font-size: 12px;">Elite Force</h4>\r\n<p style="padding: 5px 0px; margin: 0px; outline: 0px; color: #333333; float: left; font-size: 12px; line-height: 16px; width: 260px; font-family: \'Open Sans\', sans-serif;">Elite Tower, House # 3 Road # 6/A, Block-J Baridhara, Dhaka-1212</p>\r\n<p>&nbsp;</p>\r\n<p><strong style="padding: 0px; margin: 0px; outline: 0px; color: #333333; font-family: \'Open Sans\', sans-serif; font-size: 13px;">Tel:</strong><span style="color: #333333; font-family: \'Open Sans\', sans-serif; font-size: 13px;">&nbsp;9846636, 9846686, 9846686</span><br style="padding: 0px; margin: 0px; outline: 0px; color: #333333; font-family: \'Open Sans\', sans-serif; font-size: 13px;" /><strong style="padding: 0px; margin: 0px; outline: 0px; color: #333333; font-family: \'Open Sans\', sans-serif; font-size: 13px;">Mobile:</strong><span style="color: #333333; font-family: \'Open Sans\', sans-serif; font-size: 13px;">&nbsp;01613242320&nbsp;</span><br style="padding: 0px; margin: 0px; outline: 0px; color: #333333; font-family: \'Open Sans\', sans-serif; font-size: 13px;" /><strong style="padding: 0px; margin: 0px; outline: 0px; color: #333333; font-family: \'Open Sans\', sans-serif; font-size: 13px;">Fax:</strong><span style="color: #333333; font-family: \'Open Sans\', sans-serif; font-size: 13px;">&nbsp;880-2-9846575</span></p>\r\n<p style="padding: 5px 0px; margin: 0px; outline: 0px; color: #333333; float: left; font-size: 12px; line-height: 16px; width: 260px; font-family: \'Open Sans\', sans-serif;">Email : wecare@elitebd.com</p>', 'Elite Tower, House # 3 Road # 6/A, Block-J Baridhara, Dhaka-1212', 'wecare@elitebd.com', '01911543307');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `user`, `email`, `password`, `level`) VALUES
(1, 'Md Admin', 'admin', 'admin@gmail.com', '1f32aa4c9a1d2ea010adcf2348166a04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `copyright`
--

CREATE TABLE `copyright` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `copyright`
--

INSERT INTO `copyright` (`id`, `title`) VALUES
(1, 'Axiom Theme');

-- --------------------------------------------------------

--
-- Table structure for table `home_post`
--

CREATE TABLE `home_post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_post`
--

INSERT INTO `home_post` (`id`, `title`, `description`, `link`, `location`) VALUES
(1, 'About Us', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>', 'http://localhost/myAllfile/abdullah_project/single-team.php?id=5', 'photos/de1ea339b5.jpg'),
(2, 'Contact Us', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>', 'http://localhost/myAllfile/abdullah_project/sub_details.php?subPageId=21', 'photos/832614b307.jpg'),
(3, 'Contact Us', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>', 'http://localhost/myAllfile/abdullah_project/main_details.php?pageId=25', 'photos/65f493c116.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `img` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`img`, `id`) VALUES
('photos/0bb85a018c.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `main_menu`
--

CREATE TABLE `main_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_menu`
--

INSERT INTO `main_menu` (`id`, `name`, `link`) VALUES
(25, 'Contact Us', '<p><em><strong>Lorem ipsum dolor sit amet</strong></em></p>\r\n<p>phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>'),
(26, 'Our Clients', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>'),
(27, 'What We Do', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>'),
(28, 'About Us', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `site_title`
--

CREATE TABLE `site_title` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_title`
--

INSERT INTO `site_title` (`id`, `title`) VALUES
(1, 'Security Force');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `location` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`location`, `description`, `link`, `title`, `id`) VALUES
('photos/9ac7e5bf4b.jpg', 'transforming the work place', 'youtube.com', 'expert knowledge', 53),
('photos/c0e162b99a.jpg', 'Welcome To our world', 'priomnews.com', 'Hello World', 57);

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE `social` (
  `id` int(11) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `facebook`, `twitter`, `youtube`) VALUES
(1, 'https://www.facebook.com', 'https://twitter.com/?lang=en', 'https://www.youtube.com/');

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu`
--

CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL,
  `main_menu_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_menu`
--

INSERT INTO `sub_menu` (`id`, `main_menu_id`, `name`, `link`) VALUES
(21, 27, 'Training', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>'),
(19, 28, 'Why Us?', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>'),
(20, 28, 'Why Us? 2', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>'),
(22, 26, 'Our Client2', '<p>Our Client2</p>');

-- --------------------------------------------------------

--
-- Table structure for table `sub_menus_child`
--

CREATE TABLE `sub_menus_child` (
  `id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_menus_child`
--

INSERT INTO `sub_menus_child` (`id`, `sub_menu_id`, `name`, `link`) VALUES
(13, 19, 'Our Policy', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>&nbsp;</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `designation`, `description`, `location`) VALUES
(1, 'AMANDA GREEN', 'HR Advisor', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>', 'photos/948cf16169.jpg'),
(2, 'AMANDA SMITH', 'HR Advisor', '<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>', 'photos/43756c0d71.jpg'),
(4, 'EDWARD WILEY', 'HR Advisor', '<p><strong>Lorem ipsum dolor sit ame</strong></p>\r\n<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;<strong>Lorem ipsum dolor sit ame</strong></p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;<strong>Lorem ipsum dolor sit ame</strong></p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>&nbsp;<strong>Lorem ipsum dolor sit ame</strong></p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>', 'photos/88a3510b10.jpg'),
(5, 'Abdullah Quraishi', 'CEO', '<p><strong>Lorem ipsum</strong>: 01911433307</p>\r\n<p>Lorem ipsum dolor sit amet, phaedrum petentium vel te, his te audiam aliquid phaedrum. Eum ut cetero efficiendi voluptatibus. Mea an facer laudem. Augue option discere vix eu. Ei summo inimicus nec.</p>\r\n<p>Id ornatus scripserit vis, ocurreret accusamus disputationi in duo. Unum civibus torquatos per te, erant dolore numquam at vel, viderer iuvaret vituperatoribus quo et. An duo quas fierent rationibus, ex mundi dolores quo. Impetus tacimates neglegentur eu vim, id unum dicit tamquam vix.</p>\r\n<p>&nbsp;</p>\r\n<p>Vix te paulo splendide conclusionemque. Cu eos liber electram, his te maiestatis vituperata. Ea etiam inimicus complectitur quo. Ex vis putant quaerendum, civibus apeirian assueverit mei an, regione oblique per ut. Id mel enim scaevola adipisci, iusto vocibus no vim.</p>\r\n<p>&nbsp;</p>\r\n<p>Quo at nisl scripserit, laudem apeirian sed cu, in viderer probatus definitiones vim. Eum vocibus maluisset scripserit at, tincidunt comprehensam eos ea. Ei populo graeci perpetua mel, te vel quas audiam. No debet scaevola instructior per. Pro te altera volumus definiebas. Doming facilisis conclusionemque ut cum. Nam mundi utroque comprehensam ex.</p>\r\n<p>Meis vivendum eleifend eu vis, ei nec amet blandit. Nec error repudiare ad, atqui persius et sea, in semper dolorum dolores mel. Ne illud augue democritum qui. Suavitate principes efficiendi est id, eam fierent adolescens ad.</p>', 'photos/d05bd49701.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `link`) VALUES
(1, '<iframe width="560" height="315" src="https://www.youtube.com/embed/1vPfLURfkBc?list=PLSjkgOme9AvvcrSq9zCxwokQeZUsajtSt?ecver=1" frameborder="0" allowfullscreen></iframe>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `copyright`
--
ALTER TABLE `copyright`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_post`
--
ALTER TABLE `home_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_menu`
--
ALTER TABLE `main_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_title`
--
ALTER TABLE `site_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_menus_child`
--
ALTER TABLE `sub_menus_child`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `copyright`
--
ALTER TABLE `copyright`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `home_post`
--
ALTER TABLE `home_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `main_menu`
--
ALTER TABLE `main_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `site_title`
--
ALTER TABLE `site_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `social`
--
ALTER TABLE `social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `sub_menus_child`
--
ALTER TABLE `sub_menus_child`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
