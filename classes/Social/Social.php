<?php
namespace App\Social;
use App\Database\Database;
use App\Helpers\Helpers;

class Social
{
    private $facebook;
    private $twitter;
    private $youtube;

    public function social_insert($data){
        $this->facebook = Helpers::validation($data['fb']);
        $this->twitter = Helpers::validation($data['twitter']);
        $this->youtube = Helpers::validation($data['youtube']);
        $query = "INSERT INTO social(facebook,twitter,youtube) VALUES(:facebook,:twitter,:youtube) ";
        $stmt = Database::Prepare($query);
        $data = [
            ':facebook'=>$this->facebook,
            ':twitter' =>$this->twitter,
            ':youtube' =>$this->youtube
        ];
        $status = $stmt->execute($data);
        if($status)
        {
            $msg = "<span style='color: green'>social inserted Successfully.</span>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>social insert failed.</span>";
            return $msg;
        }
        }
    public function getAllSocial(){
        $sql = "SELECT * FROM social ";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public function socialUpdate($data){
        $this->facebook = Helpers::validation($data['fb']);
        $this->twitter = Helpers::validation($data['twitter']);
        $this->youtube = Helpers::validation($data['youtube']);

        $sql = "UPDATE social 
                SET 
                facebook=:facebook,
                twitter = :twitter,
                youtube = :youtube";
        $stmt = Database::Prepare($sql);
        $data = [
            ':facebook'=>$this->facebook,
            ':twitter' =>$this->twitter,
            ':youtube' =>$this->youtube
        ];
        $status =  $stmt->execute($data);
        if ($status){
            echo "<span style='color: green'>social updated Successfully.</span>";
        }else{
            echo "<span style='color: #ac2925'>Update failed.</span>";
        }
    }


}