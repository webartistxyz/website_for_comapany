<?php
namespace App\Logo;
use App\Database\Database;
use App\Helpers\Helpers;
class Logo{
    private $image;
    private $id;
    public static function getLogo(){
        $sql = "SELECT * FROM logo order by id DESC LIMIT 1";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $info = $stmt->fetch();
        return $info;
    }
    public function getLogoById($id){
        $this->id = Helpers::validation($id);
        $sql = "SELECT * FROM logo WHERE id = :id";
        $stmt = Database::Prepare($sql);
        $data = [
            ':id'=> $this->id
        ];
        $stmt->execute($data);
        $data = $stmt->fetch();
        return $data;
    }
    public function updateLogo($id, $image){
        $this->image = $image;
        $this->id = Helpers::validation($id);

        $sql = "SELECT * FROM logo WHERE id = :id";
        $stmt = Database::Prepare($sql);
        $data = [
            ':id'=> $this->id
        ];
        $stmt->execute($data);
        $data = $stmt->fetch();
        if ($data){
            $deleteLink = $data["img"];
            unlink($deleteLink);
        }

        $sql = "UPDATE logo 
            SET 
            img = :img
            WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $data = [
            ':img'=> $this->image
        ];
        $status =  $stmt->execute($data);
        if ($status){
            $_SESSION["SuccessMsg"] = "Logo updated Successfully";
            echo "<script>window.location = 'logo.php';</script>";
        }else{
            $_SESSION["ErrorMsg"] = "Logo update failed";
            echo "<script>window.location = 'logo.php';</script>";
        }
    }


}