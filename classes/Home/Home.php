<?php
namespace App\Home;
use App\Database\Database;
use App\Helpers\Helpers;
class Home{
    private $title;
    private $description;
    private $link;
    private $image;
    public function HomeInsert($data, $image){

        $this->title = Helpers::validation($data['title']);
        $this->description = $data['description'];
        $this->link = $data['link'];
        $this->image = $image;

        $query = "INSERT INTO home_post(title,description, link, location) 
VALUES (:title,:description, :link, :location)";
        $stmt = Database::Prepare($query);
        $data = [
            ':title'=> $this->title,
            ':description'=> $this->description,
            ':link'=> $this->link,
            ':location'=> $this->image
        ];
        $status =  $stmt->execute($data);
        if ($status){
            $msg = "<h4 style='color: green'>Post inserted Successfully.</h4>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Post insert failed.</span>";
            return $msg;
        }
    }

    public static function getAllHomePost(){
        $sql = "SELECT * FROM home_post ORDER BY id DESC LIMIT 4 ";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public static function getHomeById($id){
        $sql = "SELECT * FROM home_post WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public function updateHome($id,$data, $image){

        $this->title = Helpers::validation($data['title']);
        $this->description = $data['description'];
        $this->link = $data['link'];
        $this->image = $image;

        if (empty($this->image)){

            $sql = "UPDATE home_post 
                SET 
                title=:title,
                description = :description,
                link = :link
                WHERE id = '$id'";
            $stmt = Database::Prepare($sql);
            $data = [
                ':title'=> $this->title,
                ':description'=> $this->description,
                ':link'=> $this->link
            ];
            $status =  $stmt->execute($data);
            if ($status){
                $_SESSION["SuccessMsg"] = "Home post updated Successfully";
                echo "<script>window.location = 'home_post_list.php';</script>";
            }else{
                $_SESSION["ErrorMsg"] = "Home post update failed";
                echo "<script>window.location = 'home_post_list.php';</script>";
            }
        }
        else{
            $query = "SELECT * FROM home_post WHERE id = '$id'";
            $stmt = Database::Prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if ($data){
                $deleteLink = $data["location"];
                unlink($deleteLink);
            }

            $sql = "UPDATE home_post 
                SET 
                title=:title,
                description = :description,
                link = :link,
                location = :location
                WHERE id = '$id'";
            $stmt = Database::Prepare($sql);
            $data = [
                ':title'=> $this->title,
                ':description'=> $this->description,
                ':link'=> $this->link,
                ':location'=> $this->image
            ];
            $status =  $stmt->execute($data);
            if ($status){
                $_SESSION["SuccessMsg"] = "Home post updated Successfully";
                echo "<script>window.location = 'home_post_list.php';</script>";
            }else{
                $_SESSION["ErrorMsg"] = "Home post update failed";
                echo "<script>window.location = 'home_post_list.php';</script>";
            }
        }
    }

    public static function deleteHome($id)
    {
        $query = "SELECT * FROM home_post WHERE id = '$id'";
        $stmt = Database::Prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        if ($data){
            $deleteLink = $data["location"];
            unlink($deleteLink);
        }

        $sql = "DELETE FROM home_post WHERE id = '$id' LIMIT 1";
        $stmt = Database::Prepare($sql);
        $status = $stmt->execute();
        if ($status){
            $msg = "<span style='color: green'>Home post deleted Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Slider delete failed</span>";
            return $msg;
        }
    }
}