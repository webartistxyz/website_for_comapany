<?php
namespace App\CopyRight;
use App\Database\Database;
use App\Helpers\Helpers;
class CopyRight{
    private $title;

    public function Insert($data){
        $this->title = Helpers::validation($data['copy_right']);
        $query = "INSERT INTO copyright(title) VALUES(:title) ";
        $stmt = Database::Prepare($query);
        $data = [
            ':title' =>$this->title
        ];
        $status = $stmt->execute($data);
        if($status)
        {
            $msg = "<h4 style='color: green'> Inserted Successfully.</h4>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'> Insert failed.</span>";
            return $msg;
        }
    }

    public static function getTextById($id){
        $sql = "SELECT * FROM copyright WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
	public static function getTitleById($id){
		$sql = "SELECT * FROM site_title WHERE id = '$id'";
		$stmt = Database::Prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}
    public static function getText(){
        $sql = "SELECT * FROM copyright";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
	public static function getTitle(){
		$sql = "SELECT * FROM site_title";
		$stmt = Database::Prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}
    public function updateText($id,$data){

        $this->title = Helpers::validation($data['copy_right']);

        $sql = "UPDATE copyright 
            SET 
            title =:title
            WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $data = [
            ':title'=> $this->title
            ];
        $status =  $stmt->execute($data);
        if ($status){
            $_SESSION["SuccessMsg"] = "Text updated Successfully";
            echo "<script>window.location = 'copyright.php';</script>";
        }else{
            $_SESSION["ErrorMsg"] = "Text update failed";
            echo "<script>window.location = 'copyright.php';</script>";
        }
    }
	public function updateTitle($id,$data){

		$this->title = Helpers::validation($data['copy_right']);

		$sql = "UPDATE site_title 
            SET 
            title =:title
            WHERE id = '$id'";
		$stmt = Database::Prepare($sql);
		$data = [
			':title'=> $this->title
		];
		$status =  $stmt->execute($data);
		if ($status){
			$_SESSION["SuccessMsg"] = "Title updated Successfully";
			echo "<script>window.location = 'site_title.php';</script>";
		}else{
			$_SESSION["ErrorMsg"] = "Title update failed";
			echo "<script>window.location = 'site_title.php';</script>";
		}
	}
}