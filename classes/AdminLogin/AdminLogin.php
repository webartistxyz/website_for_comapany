<?php
namespace App\AdminLogin;
use App\Session\Session;
use App\Database\Database;
use App\Helpers\Helpers;
Session::checkLogin();
class AdminLogin{
    private $format;
    public function __construct()
    {
        $this->format = new Helpers();
    }
    public function admin_login($userName, $password){
        $user_name = $this->format->validation($userName);
        $pass = $this->format->validation($password);
        if(empty($user_name) || empty($pass)){
            $msg = "<span style='color: #ac2925'>Username or password must not be empty.</span>";
            return $msg;
        }else{
            $query = "SELECT * FROM admin WHERE user = '$user_name' AND password = '$pass'";
            $stmt = Database::Prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if ($data) {
                Session::set("login", true);
                Session::set("adminId", $data["id"]);
                Session::set("adminUser", $data["user"]);
                Session::set("adminName", $data["name"]);
                header("Location: dashboard.php");
            } else {
                $msg = "<span style='color: #ac2925'>Password or Username don't match!</span>";
                return $msg;
            }
        }
    }
}