<?php
namespace App\Portfolio;
use App\Database\Database;
use App\Helpers\Helpers;
class Portfolio{
	private $name;
	private $description;
	private $image;
	public function insert($data, $image){

		$this->name = Helpers::validation($data['name']);
		$this->description = $data['description'];
		$this->image = $image;

		$query = "INSERT INTO portfolio (name, description, location) 
					VALUES (:name, :description, :location)";
		$stmt = Database::Prepare($query);
		$data = [
			':name'=> $this->name,
			':description'=> $this->description,
			':location'=> $this->image
		];
		$status =  $stmt->execute($data);
		if ($status){
			$msg = "<h4 style='color: green'>Post inserted Successfully.</h4>";
			return $msg;
		}else{
			$msg = "<span style='color: #ac2925'>Post insert failed.</span>";
			return $msg;
		}
	}

	public static function getAllPortfolio(){
		$sql = "SELECT * FROM portfolio ORDER BY id DESC ";
		$stmt = Database::Prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}
	public static function getPortfolioById($id){
		$sql = "SELECT * FROM portfolio WHERE id = '$id'";
		$stmt = Database::Prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}
	public function updatePortfolio($id,$data, $image){

		$this->name = Helpers::validation($data['name']);
		$this->description = $data['description'];
		$this->image = $image;
		if (empty($this->image)){

			$sql = "UPDATE portfolio 
                SET 
                name=:name,
                description = :description
                WHERE id = '$id'";
			$stmt = Database::Prepare($sql);
			$data = [
				':name'=> $this->name,
				':description'=> $this->description
			];
			$status =  $stmt->execute($data);
			if ($status){
				$_SESSION["SuccessMsg"] = "Updated Successfully";
				echo "<script>window.location = 'portfolio_list.php';</script>";
			}else{
				$_SESSION["ErrorMsg"] = "Update failed";
				echo "<script>window.location = 'portfolio_list.php';</script>";
			}
		}
		else{
			$query = "SELECT * FROM portfolio WHERE id = '$id'";
			$stmt = Database::Prepare($query);
			$stmt->execute();
			$data = $stmt->fetch();
			if ($data){
				$deleteLink = $data["location"];
				unlink($deleteLink);
			}

			$sql = "UPDATE portfolio 
                SET 
                name=:name,
                description = :description,
                location = :location
                WHERE id = '$id'";
			$stmt = Database::Prepare($sql);
			$data = [
				':name'=> $this->name,
				':description'=> $this->description,
				':location'=> $this->image
			];
			$status =  $stmt->execute($data);
			if ($status){
				$_SESSION["SuccessMsg"] = "Updated Successfully";
				echo "<script>window.location = 'portfolio_list.php';</script>";
			}else{
				$_SESSION["ErrorMsg"] = "Update failed";
				echo "<script>window.location = 'portfolio_list.php';</script>";
			}
		}
	}

	public static function deletePortfolio($id)
	{
		$query = "SELECT * FROM portfolio WHERE id = '$id'";
		$stmt = Database::Prepare($query);
		$stmt->execute();
		$data = $stmt->fetch();
		if ($data){
			$deleteLink = $data["location"];
			unlink($deleteLink);
		}

		$sql = "DELETE FROM portfolio WHERE id = '$id' LIMIT 1";
		$stmt = Database::Prepare($sql);
		$status = $stmt->execute();
		if ($status){
			$msg = "<span style='color: green'>Deleted Successfully</span>";
			return $msg;
		}else{
			$msg = "<span style='color: #ac2925'>Delete failed</span>";
			return $msg;
		}
	}
}