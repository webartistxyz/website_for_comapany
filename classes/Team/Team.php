<?php
namespace App\Team;
use App\Database\Database;
use App\Helpers\Helpers;
class Team{
	private $name;
	private $designation;
	private $description;
	private $image;
	public function insert($data, $image){

		$this->name = Helpers::validation($data['name']);
		$this->designation = $data['designation'];
		$this->description = $data['description'];
		$this->image = $image;

		$query = "INSERT INTO team (name,designation, description, location) 
					VALUES (:name,:designation, :description, :location)";
		$stmt = Database::Prepare($query);
		$data = [
			':name'=> $this->name,
			':designation'=> $this->designation,
			':description'=> $this->description,
			':location'=> $this->image
		];
		$status =  $stmt->execute($data);
		if ($status){
			$msg = "<h4 style='color: green'>Post inserted Successfully.</h4>";
			return $msg;
		}else{
			$msg = "<span style='color: #ac2925'>Post insert failed.</span>";
			return $msg;
		}
	}

	public static function getAllTeam(){
		$sql = "SELECT * FROM team ORDER BY id DESC ";
		$stmt = Database::Prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}
	public static function geTeamById($id){
		$sql = "SELECT * FROM team WHERE id = '$id'";
		$stmt = Database::Prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}
	public function updateTeam($id,$data, $image){

		$this->name = Helpers::validation($data['name']);
		$this->designation = $data['designation'];
		$this->description = $data['description'];
		$this->image = $image;
		if (empty($this->image)){

			$sql = "UPDATE team 
                SET 
                name=:name,
                designation = :designation,
                description = :description
                WHERE id = '$id'";
			$stmt = Database::Prepare($sql);
			$data = [
				':name'=> $this->name,
				':designation'=> $this->designation,
				':description'=> $this->description
			];
			$status =  $stmt->execute($data);
			if ($status){
				$_SESSION["SuccessMsg"] = "Updated Successfully";
				echo "<script>window.location = 'team_list.php';</script>";
			}else{
				$_SESSION["ErrorMsg"] = "Update failed";
				echo "<script>window.location = 'team_list.php';</script>";
			}
		}
		else{
			$query = "SELECT * FROM team WHERE id = '$id'";
			$stmt = Database::Prepare($query);
			$stmt->execute();
			$data = $stmt->fetch();
			if ($data){
				$deleteLink = $data["location"];
				unlink($deleteLink);
			}

			$sql = "UPDATE team 
                SET 
                name=:name,
                designation = :designation,
                description = :description,
                location = :location
                WHERE id = '$id'";
			$stmt = Database::Prepare($sql);
			$data = [
				':name'=> $this->name,
				':designation'=> $this->designation,
				':description'=> $this->description,
				':location'=> $this->image
			];
			$status =  $stmt->execute($data);
			if ($status){
				$_SESSION["SuccessMsg"] = "Updated Successfully";
				echo "<script>window.location = 'team_list.php';</script>";
			}else{
				$_SESSION["ErrorMsg"] = "Update failed";
				echo "<script>window.location = 'team_list.php';</script>";
			}
		}
	}

	public static function deleteTeam($id)
	{
		$query = "SELECT * FROM team WHERE id = '$id'";
		$stmt = Database::Prepare($query);
		$stmt->execute();
		$data = $stmt->fetch();
		if ($data){
			$deleteLink = $data["location"];
			unlink($deleteLink);
		}

		$sql = "DELETE FROM team WHERE id = '$id' LIMIT 1";
		$stmt = Database::Prepare($sql);
		$status = $stmt->execute();
		if ($status){
			$msg = "<span style='color: green'>Deleted Successfully</span>";
			return $msg;
		}else{
			$msg = "<span style='color: #ac2925'>Delete failed</span>";
			return $msg;
		}
	}
}