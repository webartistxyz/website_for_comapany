<?php
/**
 * Format Class
 */
namespace App\Helpers;
class Helpers{
    public static function formatDate($date){
        return date('F j, Y, g:i a', strtotime($date));
    }

    public static function textShorten($text, $limit = 400){
        $text = $text. " ";
        $text = substr($text, 0, $limit);
        $text = substr($text, 0, strrpos($text, ' '));
        $text = $text.".....";
        return $text;
    }
    public static function textBreak($text, $limit = 20){
        $text = $text. " ";
        $text = substr($text, 0, $limit);
        $text = substr($text, 0, strrpos($text, ' '));
        $text = $text.".....";
        return $text;
    }

    public static function validation($data){
        $data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public static function title(){
        $path = $_SERVER['SCRIPT_FILENAME'];
        $title = basename($path, '.php');
        //$title = str_replace('_', ' ', $title);
        if ($title == 'index') {
            $title = 'home';
        }elseif ($title == 'contact') {
            $title = 'contact';
        }
        return $title = ucfirst($title);
    }
    public static function error($errors){
        if(!empty($errors)){
            $output = "<span style='color: #ac2925'>$errors</span><br>";
            return $output;
        }
    }
    public static function success($errors){
        if(!empty($errors)){
            $output = "<span style='color: green'>$errors</span><br>";
            return $output;
        }
    }
}
?>