<?php
namespace App\SubMenu;
use App\Database\Database;
use App\Helpers\Helpers;

class SubMenu{
    private $subMenu;
    private $menuId;
    private $link;


    public function menuInsert($data){
        $this->menuId = Helpers::validation($data['menuId']);
        $this->subMenu = Helpers::validation($data['menu']);
        $this->link = $data['link'];
        $query = "INSERT INTO sub_menu(main_menu_id, name, link) VALUES(:main_menu_id, :name, :link) ";
        $stmt = Database::Prepare($query);
        $data = [
            ':main_menu_id' =>$this->menuId,
            ':name' =>$this->subMenu,
            ':link' =>$this->link
        ];
        $status = $stmt->execute($data);
        if($status)
        {
            $msg = "<h4 style='color: green'>Menu inserted Successfully.</h4>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Menu insert failed.</span>";
            return $msg;
        }
    }
    public function subMenuInsert($data){
        $this->menuId = Helpers::validation($data['menuId']);
        $this->subMenu = Helpers::validation($data['menu']);
        $this->link = $data['link'];
        $query = "INSERT INTO sub_menus_child(sub_menu_id, name, link) VALUES(:main_menu_id, :name, :link) ";
        $stmt = Database::Prepare($query);
        $data = [
            ':main_menu_id' =>$this->menuId,
            ':name' =>$this->subMenu,
            ':link' =>$this->link
        ];
        $status = $stmt->execute($data);
        if($status)
        {
            $msg = "<h4 style='color: green'>Menu inserted Successfully.</h4>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Menu insert failed.</span>";
            return $msg;
        }
    }
    public static function getAllSubMenu(){
        $sql = "SELECT * FROM sub_menu ORDER BY id DESC ";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public static function getMenuById($id){
        $sql = "SELECT * FROM sub_menu WHERE main_menu_id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public static function getMenuByIdForEdit($id){
        $sql = "SELECT * FROM sub_menu WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public static function getChildMenuByIdForEdit($id){
        $sql = "SELECT * FROM sub_menus_child WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public static function getChildById($id){
        $sql = "SELECT * FROM sub_menus_child WHERE sub_menu_id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public function updateMenu($id,$data){

        $this->menuId = Helpers::validation($data['menuId']);
        $this->subMenu = Helpers::validation($data['menu']);
        $this->link = $data['link'];

        $sql = "UPDATE sub_menu 
            SET 
            main_menu_id = :main_menu_id,
            name =:name,
            link =:link
            WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $data = [
            ':main_menu_id'=> $this->menuId,
            ':name'=> $this->subMenu,
            ':link' =>$this->link
        ];
        $status =  $stmt->execute($data);
        if ($status){
            $_SESSION["SuccessMsg"] = "Menu updated Successfully";
            echo "<script>window.location = 'sub_menu_list.php';</script>";
        }else{
            $_SESSION["ErrorMsg"] = "Menu update failed";
            echo "<script>window.location = 'sub_menu_list.php';</script>";
        }
    }
    public function updateChildMenu($id,$data){

        $this->menuId = Helpers::validation($data['menuId']);
        $this->subMenu = Helpers::validation($data['menu']);
        $this->link = $data['link'];

        $sql = "UPDATE sub_menus_child 
            SET 
            sub_menu_id = :sub_menu_id,
            name = :name,
            link = :link
            WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $data = [
            ':sub_menu_id'=> $this->menuId,
            ':name'=> $this->subMenu,
            ':link' =>$this->link
        ];
        $status =  $stmt->execute($data);
        if ($status){
            $_SESSION["SuccessMsg"] = "Menu updated Successfully";
            echo "<script>window.location = 'sub_menus_child_list.php';</script>";
        }else{
            $_SESSION["ErrorMsg"] = "Menu update failed";
            echo "<script>window.location = 'sub_menus_child_list.php';</script>";
        }
    }
    public static function deleteMenu($id)
    {
        $sql = "DELETE FROM sub_menus_child WHERE sub_menu_id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();

        $sql = "DELETE FROM sub_menu WHERE id = '$id' LIMIT 1";
        $stmt = Database::Prepare($sql);
        $status = $stmt->execute();

        if ($status){
            $msg = "<span style='color: green'>Menu deleted Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Menu delete failed</span>";
            return $msg;
        }
    }
    public static function deleteChildMenu($id)
    {
        $sql = "DELETE FROM sub_menus_child WHERE id = '$id' LIMIT 1";
        $stmt = Database::Prepare($sql);
        $status = $stmt->execute();
        if ($status){
            $msg = "<span style='color: green'>Menu deleted Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Menu delete failed</span>";
            return $msg;
        }
    }

}