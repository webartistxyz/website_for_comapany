<?php
namespace App\Video;
use App\Database\Database;
use App\Helpers\Helpers;
class Video
{
    private $vidLink;

    public function videoInsert($data)
    {

        $this->vidLink = $data['video'];
        $query = "INSERT INTO video(link) VALUES(:link) ";
        $stmt = Database::Prepare($query);
        $data = [
            ':link' => $this->vidLink
        ];
        $status = $stmt->execute($data);
        if ($status) {
            $msg = "<h4 style='color: green'>Video inserted Successfully.</h4>";
            return $msg;
        } else {
            $msg = "<span style='color: #ac2925'>Video insert failed.</span>";
            return $msg;
        }
    }
        public function getAllLink(){
        $sql = "SELECT * FROM video ORDER BY id DESC LIMIT 4 ";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }


    public function vidUpdate($data, $id){
        $this->vidLink = $data['video'];

        $sql = "UPDATE video 
                SET 
                link = :link WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $data = [
            ':link'=>$this->vidLink,

        ];
        $status =  $stmt->execute($data);
        if ($status){
            $_SESSION["SuccessMsg"] = " Updated Successfully";
            echo "<script>window.location = 'video_list.php';</script>";
        }else{
            $_SESSION["ErrorMsg"] = "Update failed";
            echo "<script>window.location = 'video_list.php';</script>";
        }
    }
    public  function getVidLinkId($id){
        $sql = "SELECT * FROM video WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }

    public static function deleteVideo($id)
    {
        $sql = "DELETE FROM video WHERE id = '$id' LIMIT 1";
        $stmt = Database::Prepare($sql);
        $status = $stmt->execute();
        if ($status){
            $msg = "<span style='color: green'>Video post deleted Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Video delete failed</span>";
            return $msg;
        }
    }
    public function frontVideo(){
        $sql = "SELECT * FROM video ORDER BY id DESC limit 1";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }

}