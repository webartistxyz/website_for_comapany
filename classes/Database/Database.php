<?php
namespace App\Database;
$filePath = realpath(dirname(__FILE__));
include $filePath."/../../config/config.php";
use PDO;
class Database
{
    private static $pdo;
    private static function connection(){
        if (!isset(self::$pdo)){
            try{
                self::$pdo = new PDO('mysql:host='.DB_HOST.'; dbname='.DB_NAME, DB_USER, DB_PASS);
            }catch (PDOException $e){
                echo $e->getMessage();
            }
        }
        return self::$pdo;

    }
    public static function Prepare($sql){
        return self::connection()->prepare($sql);
    }

}