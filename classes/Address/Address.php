<?php
namespace App\Address;
use App\Database\Database;
use App\Helpers\Helpers;
class Address
{
    private $address;
    private $location;
    private $email;
    private $mobile;
    public function getAllAddress(){
        $sql = "SELECT * FROM address ";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public function addressUpdate($data){
        $this->address  = $data['address'];
        $this->location = $data['location'];
        $this->email    = $data['email'];
        $this->mobile   = $data['mobile'];

        $sql = "UPDATE address 
                SET 
                address  = :address,
                location = :location,
                email    = :email,
                mobile   = :mobile
                ";
        $stmt = Database::Prepare($sql);
        $data = [
            ':address'=>$this->address,
            ':location'=>$this->location,
            ':email'=>$this->email,
            ':mobile'=>$this->mobile
        ];
        $status =  $stmt->execute($data);
        if ($status){
            echo "<span style='color: #00A000'>Updated Successfully</span>";
        }else{
        	echo "<span style='color: #ac2925'>Update failed</span>.";
        }
    }


}