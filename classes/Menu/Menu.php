<?php
namespace App\Menu;
use App\Database\Database;
use App\Helpers\Helpers;
class Menu
{
    private $mainMenu;
    private $link;

    public function menuInsert($data){
        $this->mainMenu = Helpers::validation($data['menu']);
        $this->link = $data['link'];
        $query = "INSERT INTO main_menu(name, link) VALUES(:name, :link) ";
        $stmt = Database::Prepare($query);
        $data = [
            ':name' =>$this->mainMenu,
            ':link' =>$this->link
        ];
        $status = $stmt->execute($data);
        if($status)
        {
            $msg = "<h4 style='color: green'>Menu inserted Successfully.</h4>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Menu insert failed.</span>";
            return $msg;
        }
    }
    /*public static function getAllMenuWithSub(){
        $sql = "SELECT main_menu.name AS main_name, sub_menu.name AS sub_name, sub_menus_child.name AS child
                FROM main_menu 
                LEFT JOIN sub_menu 
                ON main_menu.id=sub_menu.main_menu_id 
                LEFT JOIN sub_menus_child
                ON sub_menu.id=sub_menus_child.sub_menu_id
                ORDER BY main_menu.id DESC";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }*/
    public static function getAllMenu(){
        $sql = "SELECT * FROM main_menu ORDER BY id DESC ";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public static function getMenuById($id){
        $sql = "SELECT * FROM main_menu WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public function updateMenu($id,$data){

        $this->mainMenu = Helpers::validation($data['menu']);
        $this->link = $data['link'];

        $sql = "UPDATE main_menu 
            SET 
            name =:name,
            link =:link
            WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $data = [
            ':name'=> $this->mainMenu,
            ':link' =>$this->link
        ];
        $status =  $stmt->execute($data);
        if ($status){
            $_SESSION["SuccessMsg"] = "Menu updated Successfully";
            echo "<script>window.location = 'menu_list.php';</script>";
        }else{
            $_SESSION["ErrorMsg"] = "Menu update failed";
            echo "<script>window.location = 'menu_list.php';</script>";
        }
    }
    public static function deleteMenu($id)
    {
        $sql = "SELECT * FROM sub_menu WHERE main_menu_id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        $delId = $data["id"];

        $sql = "DELETE FROM sub_menus_child WHERE sub_menu_id = '$delId'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();

        $sql = "DELETE FROM sub_menu WHERE main_menu_id = '$id' LIMIT 1";
        $stmt = Database::Prepare($sql);
        $status1 = $stmt->execute();

        $sql = "DELETE FROM main_menu WHERE id = '$id' LIMIT 1";
        $stmt = Database::Prepare($sql);
        $status = $stmt->execute();



        if ($status){
            $msg = "<span style='color: green'>Menu deleted Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Menu delete failed</span>";
            return $msg;
        }
    }


}