<?php
namespace App\Slider;
use App\Database\Database;
use App\Helpers\Helpers;
class Slider{
    private $title;
    private $description;
    private $link;
    private $image;
    public function sliderInsert($data, $image){

        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->link = $data['link'];
        $this->image = $image;

        $query = "INSERT INTO slider(title, link, description, location) 
VALUES (:title, :link, :description, :location)";
        $stmt = Database::Prepare($query);
        $data = [
            ':title'=> $this->title,
            ':link'=> $this->link,
            ':description'=> $this->description,
            ':location'=> $this->image
        ];
        $status =  $stmt->execute($data);
        if ($status){
            $msg = "<h4 style='color: green'>Slider inserted Successfully.</h4>";
            return $msg;
        }else{
            $msg = "<span style='color: #ac2925'>Slider insert failed.</span>";
            return $msg;
        }
    }

    public static function getAllSlider(){
        $sql = "SELECT * FROM slider ORDER BY id DESC LIMIT 4 ";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
    public static function getSliderById($id){
        $sql = "SELECT * FROM slider WHERE id = '$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }
    public function updateSlider($id,$data, $image){

        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->link = $data['link'];
        $this->image = $image;

            if (empty($this->image)){

                $sql = "UPDATE slider 
                SET 
                title=:title,
                link = :link,
                description = :description
                WHERE id = '$id'";
                $stmt = Database::Prepare($sql);
                $data = [
                    ':title'=> $this->title,
                    ':link'=> $this->link,
                    ':description'=> $this->description
                ];
                $status =  $stmt->execute($data);
                if ($status){
                    $_SESSION["SuccessMsg"] = "Slider updated Successfully";
                    echo "<script>window.location = 'slider.php';</script>";
                }else{
                    $_SESSION["ErrorMsg"] = "Slider update failed";
                    echo "<script>window.location = 'slider.php';</script>";
                }
            }
            else{
                $query = "SELECT * FROM slider WHERE id = '$id'";
                $stmt = Database::Prepare($query);
                $stmt->execute();
                $data = $stmt->fetch();
                if ($data){
                    $deleteLink = $data["location"];
                    unlink($deleteLink);
                }

                $sql = "UPDATE slider 
                SET 
                title=:title,
                link = :link,
                description = :description,
                location = :location
                WHERE id = '$id'";
                $stmt = Database::Prepare($sql);
                $data = [
                    ':title'=> $this->title,
                    ':link'=> $this->link,
                    ':description'=> $this->description,
                    ':location'=> $this->image
                ];
                $status =  $stmt->execute($data);
                if ($status){
                    $_SESSION["SuccessMsg"] = "Slider updated Successfully";
                    echo "<script>window.location = 'slider.php';</script>";
                }else{
                    $_SESSION["ErrorMsg"] = "Slider update failed";
                    echo "<script>window.location = 'slider.php';</script>";
                }
            }
        }

    public static function deleteSlider($id)
    {
        $query = "SELECT * FROM slider WHERE id = '$id'";
        $stmt = Database::Prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        if ($data){
            $deleteLink = $data["location"];
            unlink($deleteLink);
        }

        $sql = "DELETE FROM slider WHERE id = '$id' LIMIT 1";
        $stmt = Database::Prepare($sql);
        $status = $stmt->execute();
        if ($status){
            $_SESSION["SuccessMsg"] = "Slider deleted Successfully";
            echo "<script>window.location = 'slider.php';</script>";
        }else{
            $_SESSION["ErrorMsg"] = "Slider delete failed";
            echo "<script>window.location = 'slider.php';</script>";
        }
    }
    }