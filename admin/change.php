<?php include 'header.php';?>
<?php
use App\Session\Session;
use App\Database\Database;
?>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->           
				<style>
				.active span{ color:red}
				 .head{font-size:21px; color:#09F;}
				 .spanhead{margin-left:20px}
				 .treeview-menu > li > a:hover{ color:#0099FF !important}
				 .arrowhead{margin-top:10px}
				</style>
				<?php include 'menu.php';?>
			</section>                <!-- /.sidebar -->
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		  <aside class="right-side" >
			<!-- Content Header (Page header) -->
			<!-- Main content -->
			<section class="content">

				<!-- Small boxes (Stat box) -->
				<div class="row">
		   
					<!-- ./col -->
				</div><!-- /.row -->

				<!-- top row -->
				<div class="row">
		  
					<!-- left column -->
						<div class="col-md-12">
							<!-- general form elements -->
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Change Admin Password</h3>
                                </div><!-- /.box-header -->
								<?php
		if (!empty($_GET['message']) && $_GET['message'] == 'old') {
			echo '<div class="alert alert-success">' ;
			echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
			echo '<h4>Your Old Password Do not Match</h4>';
			echo '</div>';
		}
		else if (!empty($_GET['message']) && $_GET['message'] == 'new') {
			echo '<div class="alert alert-success">' ;
			echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
			echo '<h4>Your New Password Do not Match</h4>';
			echo '</div>';
		}
		else if (!empty($_GET['message']) && $_GET['message'] == 'success') {
			echo '<div class="alert alert-success">' ;
			echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
			echo '<h4>Your Password Successfully Changed.</h4>';
			echo '</div>';
		}
		else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
			echo '<div class="alert alert-success">' ;
			echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
			echo '<h4>Your Password Change Failed Please Try Later ! </h4>';
			echo '</div>';
		}
		 
	?>	
								
		<?php
		

		if(isset($_POST['password']))
		{
  			 
	    $id = Session::get('adminId');
		$old = md5(md5($_POST['old']));
		$username=$_POST['username'];
 		$new=$_POST['new'];
		$rnew=$_POST['rnew'];
		$sql="select * from admin where id='$id'";
        $stmt = Database::Prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        $pass = $data['password'];

		if($old==$pass)
		{
			if($new==$rnew)
			{
			    $encryptPass = md5(md5($new));

                //$pas="update login set password='$new' where username='$login_session'";

                $sql = "UPDATE admin 
                SET 
                password  = :password,
                user = :user
                WHERE id = '$id' ";
                $stmt = Database::Prepare($sql);
                $data = [
                    ':password'=> $encryptPass,
                    ':user'=> $username

                ];
                $status =  $stmt->execute($data);


		if ($status) {
				echo"<script>location.href='change.php?message=success'</script>";
				Session::destroy();
		} 
		else {
			echo"<script>location.href='change.php?message=error'</script>";
		}
		 }
		 else
		{
				 echo"<script>location.href='change.php?message=new'</script>";
		}
		}
		
		 else{
			 echo"<script>location.href='change.php?message=old'</script>";
		 }
		}
 		 
		?>
									<!-- form start -->
	<form role="form" name="entryform" id="entryform" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" enctype="multipart/form-data">
		<div class="box-body">                                        
	 <h2>Change Admin Password*</h2>
			<div class="form-group">
				<label>Enter Old Password*</label>
				<input type="password" class="form-control" name="old" required/>  
			</div>
            <div class="form-group">
                <label>username *</label>
                <input type="text" class="form-control" name="username" value="<?= Session::get('adminUser'); ?>" required/>
            </div>
			<div class="form-group">
				<label>Enter New Password*</label>
				<input type="password" class="form-control" name="new" required/>  
			</div>
			<div class="form-group">
				<label>Re-Enter New Password*</label>
				<input type="password" class="form-control" name="rnew" required/>  
			</div>
		  
			 
		</div><!-- /.box-body -->

		<div class="box-footer">						   
			
		<button type="submit" name="password" class="btn btn-success" style="width:100px"> Submit </button>
		</div>
	</form>
		 
							</div><!-- /.box -->
							
							<!-- Input addon -->
							<!-- /.box -->
		
						</div><!--/.col (left) -->
				</div>             
				<!-- /.row -->
			</section><!-- /.content -->
		</aside>
		<!-- /.right-side -->
<?php include 'footer.php';?>