<?php include 'header.php';?>
<?php
use App\Database\Database;
use App\Team\Team;
$Obj = new Team();
$errors = array();
?>
	<div class="wrapper row-offcanvas row-offcanvas-left">
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="left-side sidebar-offcanvas">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- Sidebar user panel -->
			<style>
				.active span{ color:red}
				.head{font-size:21px; color:#09F;}
				.spanhead{margin-left:20px}
				.treeview-menu > li > a:hover{ color:#0099FF !important}
				.arrowhead{margin-top:10px}
			</style>
			<?php include 'menu.php';?>
		</section>                <!-- /.sidebar -->
	</aside>

	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side" >
		<!-- Content Header (Page header) -->
		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">

				<!-- ./col -->
			</div><!-- /.row -->

			<!-- top row -->
			<div class="row">

				<!-- left column -->
				<div class="col-md-12">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Add New Record</h3>
						</div><!-- /.box-header -->
						<?php
						if ($_SERVER["REQUEST_METHOD"] == "POST") {
							if (isset($_POST["submit"])) {

								empty($_POST["name"]) ? $errors['nameEmpty'] = "<span style='color: #ac2925'>Field must not be empty!</span>":NULL;

								empty($_POST["designation"]) ? $errors['designationEmpty'] = "<span style='color: #ac2925'>Field must not be empty!</span>":NULL;

								$file = $_FILES;
								$permited  = array('jpg', 'jpeg', 'png', 'gif');
								$file_name = $file['image']['name'];
								$file_size = $file['image']['size'];
								$file_temp = $file['image']['tmp_name'];

								$div = explode('.', $file_name);
								$file_ext = strtolower(end($div));
								$unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
								$uploaded_image = "photos/".$unique_image;

								if ($file_size >1048567) {
									$errors['imageSize'] = "<span style='color: #ac2925'>Image Size should be less then 1MB!</span>";
								}

								if (!empty($file_name) && in_array($file_ext, $permited) === false) {
									$errors['imageFormat'] = "<span style='color: #ac2925'>You can upload only:-".implode(', ', $permited)."</span>";
								} else{
									move_uploaded_file($file_temp, $uploaded_image);
									$id = $_GET["id"];
									if (empty($file_name)){
										empty($errors) ?  $Obj->updateTeam($id, $_POST, $file_name) : NULL;
									}else{
										empty($errors) ?  $Obj->updateTeam($id, $_POST, $uploaded_image) : NULL;
									}

								}
							}
						}

						?>
						<?php
						isset($_GET["id"]) ? $id = $_GET["id"] : header("Location: team_list.php");
						$info = $Obj::geTeamById($id);
						?>
						<!-- form start -->
						<form role="form" name="entryform" id="entryform" action="" method="POST" enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<label>Name *</label>
									<input class="form-control" name="name" value="<?php echo $info['name']; ?>"
									       required/>
									<?php echo !empty($errors['nameEmpty']) ? $errors['nameEmpty'] : NULL; ?>
								</div>
								<div class="form-group">
									<label>Designation *</label>
									<input class="form-control" name="designation" value="<?php echo $info['designation']; ?>" required>
									<?php echo !empty($errors['designationEmpty']) ? $errors['designationEmpty'] : NULL; ?>
								</div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control tinymce" name="description"><?php echo $info['description']; ?></textarea>
                                </div>
								<div class="form-group">
									<img src="<?php echo $info['location'];?>" alt="<?php echo $info['name'];?>"
									     width="200" height="120"/>
								</div>
								<div class="form-group">
									<label>Slider Image * <small> (Image size Must be width: 1920px & Height: 1240px)</small></label>
									<input type="file" name="image"/>
									<?php echo !empty($errors['imageSize']) ? $errors['imageSize'] : NULL; ?>
									<?php echo !empty($errors['imageFormat']) ? $errors['imageFormat'] : NULL; ?>
								</div>

							</div><!-- /.box-body -->

							<div class="box-footer">

								<button type="submit" name="submit" class="btn btn-success" style="width:100px"> Submit
								</button>
							</div>
						</form>

					</div><!-- /.box -->

					<!-- Input addon -->
					<!-- /.box -->

				</div><!--/.col (left) -->
			</div>
			<!-- /.row -->
		</section><!-- /.content -->
	</aside>
	<!-- /.right-side -->
<?php include 'footer.php';?>