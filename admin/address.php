<?php include 'header.php';?>
<?php
use App\Address\Address;
$address = new Address();

?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <style>
                .active span{ color:red}
                .head{font-size:21px; color:#09F;}
                .spanhead{margin-left:20px}
                .treeview-menu > li > a:hover{ color:#0099FF !important}
                .arrowhead{margin-top:10px}
            </style>
            <?php include 'menu.php';?>
        </section>                <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side" >
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
            </div><!-- /.row -->

            <!-- top row -->
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Update Record</h3>
                        </div><!-- /.box-header -->
                        <?php
                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            if (isset($_POST["submit"])) {

                                empty($_POST["address"]) ? $errors['addEmpty'] = "<span style='color: #ac2925'>Field must not be empty!</span>":NULL;
                                empty($_POST["location"]) ? $errors['locationEmpty'] = "<span style='color: #ac2925'>Field must not be empty!</span>":NULL;
                                empty($_POST["email"]) ? $errors['emailEmpty'] = "<span style='color: #ac2925'>Field must not be empty!</span>":NULL;
                                empty($_POST["mobile"]) ? $errors['mobileEmpty'] = "<span style='color: #ac2925'>Field must not be empty!</span>":NULL;

                                empty($errors) ?  $address->addressUpdate($_POST) : NULL;

                            }

                        }

                        $links = $address->getAllAddress();
                        if(isset($links))
                        {




                            ?>
                            <!-- form start -->
                            <form role="form" name="entryform" id="entryform" action="" method="POST">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Detail Address For Right Sidebar *</label>
                                       <textarea class="tinymce" name="address"><?php echo $links['address']?></textarea>
                                        <?php echo !empty($errors['addEmpty']) ? $errors['addEmpty'] : NULL; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Add Location For Footer *</label>
                                        <input class="form-control" name="location"
                                               value="<?php echo $links['location']?>" required/>
		                                <?php echo !empty($errors['locationEmpty']) ? $errors['menuEmpty'] : NULL; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Add Email For Footer *</label>
                                        <input class="form-control" name="email"
                                               value="<?php echo $links['email']?>" required/>
		                                <?php echo !empty($errors['emailEmpty']) ? $errors['menuEmpty'] : NULL; ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Add Mobile No For Footer *</label>
                                        <input class="form-control" name="mobile"
                                               value="<?php echo $links['mobile']?>" required/>
		                                <?php echo !empty($errors['mobileEmpty']) ? $errors['menuEmpty'] : NULL; ?>
                                    </div>
                                </div><!-- /.box-body -->

                                <div class="box-footer">

                                    <button type="submit" class="btn btn-success" style="width:100px" name="submit"> Submit
                                    </button>
                                </div>
                            </form>
                        <?php }?>

                    </div><!-- /.box -->

                    <!-- Input addon -->
                    <!-- /.box -->

                </div><!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
    </aside>
    <!-- /.right-side -->
<?php include 'footer.php';?>