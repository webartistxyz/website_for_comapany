</div><!-- ./wrapper -->
<!-- add new calendar event modal -->
<!-- jQuery 2.0.2 -->
		 <!--For Date Picker-->
	<link rel="stylesheet" href="js/jquery-ui.css">
	<script src="js/jquery-ui.js"></script>
	<script>
		$(function() {

		$( "#datepicker" ).datepicker({

		 dateFormat: 'yy-mm-dd',
		 changeMonth: true,
		 changeYear: true });
		});

		$(function() {
		$( "#datepicker2" ).datepicker({
		 dateFormat: 'yy-mm-dd',
		 changeMonth: true,
		 changeYear: true });
		});
	</script>
	<script>
		$(function() {
		$( "#from" ).datepicker({
		  defaultDate: "+1w",
		  changeMonth: true,
		  numberOfMonths: 3,
		  onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
		  }
		});
		$( "#to" ).datepicker({
		  defaultDate: "+1w",
		  changeMonth: true,
		  numberOfMonths: 3,
		  onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		  }
		});
		});
	</script>
	<!-- jQuery UI 1.10.3 -->
	<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Morris.js charts -->
	<script src="js/raphael-min.js"></script>
	<script src="js/morris.min.js" type="text/javascript"></script>
	<!-- Sparkline -->
	<script src="js/jquery.sparkline.min.js" type="text/javascript"></script>
	<!-- jvectormap -->
	<script src="js/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
	<script src="js/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
	<!-- fullCalendar -->
	<script src="js/fullcalendar.min.js" type="text/javascript"></script>
	<!-- jQuery Knob Chart -->
	<script src="js/jquery.knob.js" type="text/javascript"></script>
	<!-- daterangepicker -->
	<script src="js/daterangepicker.js" type="text/javascript"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="js/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
	<!-- iCheck -->
	<script src="js/icheck.min.js" type="text/javascript"></script>

	<!-- AdminLTE App -->
	<script src="js/app.js" type="text/javascript"></script>

	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="js/dashboard.js" type="text/javascript"></script>
	<script src="js/my_js/tinymce/tinymce.min.js" type="text/javascript"></script>
	<script src="js/my_js/tinymce/init-tinymce.js" type="text/javascript"></script>
</body>
</html>
<?php
ob_end_flush();
?>