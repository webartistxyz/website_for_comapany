<?php include 'header.php';?>
<?php
use App\Database\Database;
use App\Session\Session;
use App\Home\Home;
use App\Helpers\Helpers;
?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <style>
                .active span{ color:red}
                .head{font-size:21px; color:#09F;}
                .spanhead{margin-left:20px}
                .treeview-menu > li > a:hover{ color:#0099FF !important}
                .arrowhead{margin-top:10px}
            </style>
            <?php include 'menu.php';?>
        </section>                <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side" >
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
            </div><!-- /.row -->

            <!-- top row -->
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <br>
                        <div class="box-header text-center">
                            <h1 class="box-title">Home Posts List</h1>
                            <a class='btn btn-success pull-right' href="home_post.php">Add New</a>
                        </div>
                        <?php
                        echo Session::SuccessMsg();
                        echo Session::ErrorMsg();
                        ?>
                        <?php
                        if (isset($_GET["id"])){
                            $id=$_GET['id'];
                            $deleteMsg = Home::deleteHome($id);
                            echo isset($deleteMsg) ? $deleteMsg :NULL;

                        }
                        ?>
                        <?php
                        $sql = "SELECT * FROM home_post order by id DESC";
                        $stmt = Database::Prepare($sql);
                        $stmt->execute();
                        $result = $stmt->fetchAll();
                        ?>

                        <table id="table_info" class="table table-bordered table-striped">
                            <thead>
                                <tr class = "success">
                                    <th width="5%">No.</th>
                                    <th width="20%">Picture</th>
                                    <th width="10%">Title</th>
                                    <th width="30%">Description</th>
                                    <th width="25">Link</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $x = 0;
                            foreach ($result as $info)
                            {
                                $x++;
                                ?>
                                <tr class="warning">
                                    <td><?= $x; ?></td>
                                    <td><img src="<?php echo $info['location'];?>" alt="<?php echo $info['title'];?>" width="200" height="120"/></td>
                                    <td><?php echo $info['title'];?></td>
                                    <td><?php echo Helpers::textShorten($info['description'], 200);?></td>
                                    <td width="25%"><?php echo $info['link'];?></td>
                                    <td>
                                        <span class="pull-right">
                                        <a class="btn btn-primary" href="edit_home_post.php?id=<?php echo $info['id'];?>" title="Edit"><i class="fa
                                        fa-pencil-square-o"></i></a>
                                        <a class="btn btn-danger" href="?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" title="Delete"><i class="fa fa-times-circle"></i></a>
                                        </span>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div><!-- /.box -->

                    <!-- Input addon -->
                    <!-- /.box -->

                </div><!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
    </aside>
    <!-- /.right-side -->
    <script>
        $(document).ready(function(){
            $('#table_info').DataTable();
        });
    </script>
<?php include 'footer.php';?>