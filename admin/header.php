<?php
ob_start();
include "../vendor/autoload.php";
use App\Session\Session;
Session::checkSession();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin Panel </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <link href="css/login.css" rel="stylesheet" type="text/css" />
        <!--data table-->
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
        <script src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/my_js/data_table/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/my_js/data_table/dataTables.bootstrap.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <![endif]-->

    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="dashboard.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <img src="images/logo.png" height="35" width="194px"/>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Menus</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
						
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class=" user user-menu">
                            <a><i class="glyphicon glyphicon-user"></i>
                                Hello <?= Session::get('adminUser'); ?>
                            </a>
                        </li>
	                    <?php
	                    if (isset($_GET['action']) && $_GET['action'] == "logout"){
		                    Session::destroy();
	                    }
	                    ?>
                        <li class="user user-menu">
                        
                          <a href="?action=logout" >Sign out</a>
                      
                        </li>
                        <li class="user user-menu">
                        
                       
                         <a href="../index.php"  target="_blank">View WebSite</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>