<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
	<li class="active" >
		<a href="dashboard.php">
			<span class="spanhead" style="color:green;"><i class="fa fa-tachometer"></i> Dashboard</span>
		</a>
	</li>


    <li class="treeview ">
        <a href="#">
            <span class="spanhead"><i class="fa fa-bars"></i> Company Logo</span>
            <i class="fa fa-angle-left pull-right arrowhead"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="logo.php"><i class="fa fa-plus-square-o"></i> Update logo</a></li>
        </ul>
    </li>
    <li class="treeview ">
        <a href="#">
            <span class="spanhead"><i class="fa fa-bars"></i> Company Address</span>
            <i class="fa fa-angle-left pull-right arrowhead"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="address.php"><i class="fa fa-plus-square-o"></i> Update Address</a></li>
        </ul>
    </li>

    <li class="treeview ">
		<a href="#">
			<span class="spanhead"><i class="fa fa-bars"></i> Home Post</span>
			<i class="fa fa-angle-left pull-right arrowhead"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="home_post_list.php"><i class="fa fa-list"></i> Post List</a></li>
			<li><a href="home_post.php"><i class="fa fa-plus-square-o"></i> Add Post</a></li>
		</ul>
	</li>


 
	<li class="treeview ">
		<a href="#">
			<span class="spanhead"><i class="fa fa-bars"></i> Slider</span>
			<i class="fa fa-angle-left pull-right arrowhead"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="slider.php"><i class="fa fa-list"></i> Slider List</a></li>	
			<li><a href="slider_add.php"><i class="fa fa-plus-square-o"></i> Add Slider</a></li>		
		</ul>
	</li>

		<li class="treeview ">
		<a href="#">
			<span class="spanhead"><i class="fa fa-bars"></i> Menu</span>
			<i class="fa fa-angle-left pull-right arrowhead"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="menu_list.php"><i class="fa fa-list"></i> Menu List</a></li>
			<li><a href="menu_add.php"><i class="fa fa-plus-square-o"></i> Add Menu</a></li>
            <li><a href="sub_menu_list.php"><i class="fa fa-list"></i>Sub Menu List</a></li>
            <li><a href="sub_menu_add.php"><i class="fa fa-plus-square-o"></i> Add Sub Menu</a></li>
            <li><a href="sub_menus_child_list.php"><i class="fa fa-list"></i>Child Menu List</a></li>
            <li><a href="sub_menus_child.php"><i class="fa fa-plus-square-o"></i> Add Sub Menu's Child</a></li>
		</ul>
	</li>

    <li class="treeview ">
        <a href="#">
            <span class="spanhead"><i class="fa fa-bars"></i> Team Info</span>
            <i class="fa fa-angle-left pull-right arrowhead"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="team.php"><i class="fa fa-list"></i> Add Team Member</a></li>
            <li><a href="team_list.php"><i class="fa fa-list"></i> Team List</a></li>
        </ul>
    </li>
    <li class="treeview ">
        <a href="#">
            <span class="spanhead"><i class="fa fa-bars"></i> Activities</span>
            <i class="fa fa-angle-left pull-right arrowhead"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="portfolio.php"><i class="fa fa-list"></i> Add Activities</a></li>
            <li><a href="portfolio_list.php"><i class="fa fa-list"></i> Activity List</a></li>
        </ul>
    </li>
    <li class="treeview ">
        <a href="#">
            <span class="spanhead"><i class="fa fa-bars"></i> Video Link</span>
            <i class="fa fa-angle-left pull-right arrowhead"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="videoUpdate.php"><i class="fa fa-plus-square-o"></i> Update Video Link</a></li>
        </ul>
    </li>
    <li class="treeview ">
        <a href="#">
            <span class="spanhead"><i class="fa fa-bars"></i> Social Link</span>
            <i class="fa fa-angle-left pull-right arrowhead"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="social_edit.php"><i class="fa fa-plus-square-o"></i> Update social Link</a></li>
        </ul>
    </li>
    <li class="treeview ">
        <a href="#">
            <span class="spanhead"><i class="fa fa-bars"></i> Copyright & Title</span>
            <i class="fa fa-angle-left pull-right arrowhead"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="copyright.php"><i class="fa fa-plus-square-o"></i> Update Copyright Info</a></li>
            <li><a href="site_title.php"><i class="fa fa-plus-square-o"></i> Update Site Title</a></li>
        </ul>
    </li>

	<li class="treeview ">
		<a href="#">
			<span class="spanhead"><i class="fa fa-bars"></i> Setting</span>
			<i class="fa fa-angle-left pull-right arrowhead"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="change.php"><i class="fa fa-list"></i> Change Password</a></li>
			<li><a href="logout.php"><i class="fa fa-list"></i> Logout</a></li>	
			 		
		</ul>
	</li>
	
</ul>