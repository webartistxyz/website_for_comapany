<?php include 'header.php';?>        
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->           
				<style>
				.active span{ color:red}
				 .head{font-size:21px; color:#09F;}
				 .spanhead{margin-left:20px}
				 .treeview-menu > li > a:hover{ color:#0099FF !important}
				 .arrowhead{margin-top:10px}
				</style>
				<?php include 'menu.php';?>
			</section>                <!-- /.sidebar -->
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		  <aside class="right-side" >
			<!-- Content Header (Page header) -->
			<!-- Main content -->
			<section class="content">

				<!-- Small boxes (Stat box) -->
				<div class="row">
		   
					<!-- ./col -->
				</div><!-- /.row -->

				<!-- top row -->
				<div class="row">
		  
					<!-- left column -->
						<div class="col-md-12">
							<!-- general form elements -->
							<div class="box box-primary">	
								<?php
									if (!empty($_GET['message']) && $_GET['message'] == 'success') {
										echo '<div class="alert alert-success">' ;
										echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
										echo '<h4>Your Data Successfully Inserted</h4>';
										echo '</div>';
									}
									else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
										echo '<div class="alert alert-success">' ;
										echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
										echo '<h4>Your Data Successfully Updated</h4>';
										echo '</div>';
									}
									else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
										echo '<div class="alert alert-success">' ;
										echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
										echo '<h4>Your Data Successfully Deleted</h4>';
										echo '</div>';
									}
									else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
										echo '<div class="alert alert-success">' ;
										echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
										echo '<h4>Your Data Uploaded Error ! </h4>';
										echo '</div>';
									}
								?>
								<?php
									$num_rec_per_page=20;
									if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
									$start_from = ($page-1) * $num_rec_per_page;
									$result = mysql_query("SELECT * FROM social order by serial DESC LIMIT $start_from, $num_rec_per_page") or die(mysql_error());  
									
									echo "<table class=\"table table-bordered\">";
									echo "<tr class=\"success\">
										<th>Name</th>
										<th>Icon</th>
										<th>Link</th>
										<th><a class='btn btn-success pull-right' href=\"social_add.php\">Add New</></th></tr>";
									
									while($info = mysql_fetch_array( $result )) 
									{	
									?>									
										<tr class="warning">
											<td><?php echo $info['name'];?></td>
											<td><i class="fa fa-<?php echo $info['icon'];?>"></i></td>
											<td><?php echo $info['link'];?></td>
											
											<td>
												<span class="pull-right">
												<a class="btn btn-primary" href="social_edit.php?serial=<?php echo $info['serial'];?>" title="Edit"><i class="fa fa-pencil-square-o"></i></a> 
												<a class="btn btn-danger" href="social_delete.php?serial=<?php echo $info['serial'];?>" onclick="return confirm('Are you sure?')" title="Delete"><i class="fa fa-times-circle"></i></a>
												</span>
											</td>
										</tr>

								<?php }?>										
								</table>
								<?php 
									$sql = "SELECT * FROM social"; 
									$rs_result = mysql_query($sql); //run the query
									$total_records = mysql_num_rows($rs_result);  //count number of records
									$total_pages = ceil($total_records / $num_rec_per_page); 
									
									echo "<a style='padding:5px 10px; margin:0 1px; background:#000066;text-decoration:none; color:#fff; hover-color:#000066' href='?page=1'>".'First Page '."</a> "; // Goto 1st page  
									
									for ($i=1; $i<=$total_pages; $i++) { 
												echo "<a style=' padding:5px 10px; margin:0 1px; background:#660000;text-decoration:none;color:#fff;' href='?page=".$i."'>".$i."</a>"; 
									}; 
									echo "<a style='padding:5px 10px; margin:0 1px; background:#000066;text-decoration:none;color:#fff;'  href='?page=$total_pages'>".'Last Page'."</a>"; // Goto last page
								?>
								
							</div><!-- /.box -->
							
							<!-- Input addon -->
							<!-- /.box -->
		
						</div><!--/.col (left) -->
				</div>             
				<!-- /.row -->
			</section><!-- /.content -->
		</aside>
		<!-- /.right-side -->
<?php include 'footer.php';?>