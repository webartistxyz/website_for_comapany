<?php include 'header.php';?>        
<div class="wrapper row-offcanvas row-offcanvas-left">
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="left-side sidebar-offcanvas">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- Sidebar user panel -->           
			<style>
				.active span{ color:red}
				.head{font-size:21px; color:#09F;}
				.spanhead{margin-left:20px}
				.treeview-menu > li > a:hover{ color:#0099FF !important}
				.arrowhead{margin-top:10px}
			</style>
			<?php include 'menu.php';?>
		</section><!-- /.sidebar -->
	</aside>

	<!-- Right side column. Contains the navbar and content of the page -->
	  <aside class="right-side" >
		<!-- Content Header (Page header) -->
		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
	   
				<!-- ./col -->
			</div><!-- /.row -->

			<!-- top row -->
			<div class="row"> 
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-aqua">
						<div class="inner">
							<?php
								/*$query = "SELECT COUNT(id) FROM slider";
									 
								$result = mysql_query($query) or die(mysql_error());

								while($row = mysql_fetch_array($result)){
									echo "<h3>". $row['COUNT(id)'] ."</h3>";
									echo "<p>Total Slider</p>";
								}*/
							?>
						</div>
						<div class="icon">
							<i class="fa fa-users"></i>
						</div>
						<a href="slider.php" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div> 
				
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-green">
						<div class="inner">
							<?php
								/*$query = "SELECT COUNT(id) FROM product";
									 
								$result = mysql_query($query) or die(mysql_error());

								while($row = mysql_fetch_array($result)){
									echo "<h3>". $row['COUNT(id)'] ."</h3>";
									echo "<p>Total Product</p>";
								}*/
							?>
						</div>
						<div class="icon">
							<i class="fa fa-users"></i>
						</div>
						<a href="product.php" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
									  
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-yellow">
						<div class="inner">
							<?php
								/*$query = "SELECT COUNT(id) FROM gallery";
									 
								$result = mysql_query($query) or die(mysql_error());

								while($row = mysql_fetch_array($result)){
									echo "<h3>". $row['COUNT(id)'] ."</h3>";
									echo "<p>Total Gallery</p>";
								}*/
							?>
						</div>
						<div class="icon">
							<i class="fa fa-users"></i>
						</div>
						<a href="gallery.php" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
													  
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-yellow">
						<div class="inner">
							<?php
								/*$query = "SELECT COUNT(id) FROM client";
									 
								$result = mysql_query($query) or die(mysql_error());

								while($row = mysql_fetch_array($result)){
									echo "<h3>". $row['COUNT(id)'] ."</h3>";
									echo "<p>Total Clients</p>";
								}*/
							?>
						</div>
						<div class="icon">
							<i class="fa fa-users"></i>
						</div>
						<a href="clients.php" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
		 
				
			</div>             
			<!-- /.row -->
		</section><!-- /.content -->
	</aside>
	<!-- /.right-side -->
<?php include 'footer.php';?>