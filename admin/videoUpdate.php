<?php include 'header.php';?>
<?php
use App\Video\Video;
$video = new Video();

?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <style>
                .active span{ color:red}
                .head{font-size:21px; color:#09F;}
                .spanhead{margin-left:20px}
                .treeview-menu > li > a:hover{ color:#0099FF !important}
                .arrowhead{margin-top:10px}
            </style>
            <?php include 'menu.php';?>
        </section>                <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side" >
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
            </div><!-- /.row -->

            <!-- top row -->
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Add New Record</h3>
                        </div><!-- /.box-header -->
                        <?php
                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            if (isset($_POST["submit"])) {
                                $id = $_GET['id'];
                                empty($_POST["video"]) ? $errors['vidEmpty'] = "<span style='color: #ac2925'>Video link must not be empty!</span>":NULL;
                                empty($errors) ?  $video->vidUpdate($_POST, $id) : NULL;

                            }

                        }

                        if(isset($_GET['id']))
                        {
                            $updateId = $_GET['id'];
                            $link = $video->getVidLinkId($updateId);
                        ?>
                        <!-- form start -->

                        <form role="form" name="entryform" id="entryform" action="" method="POST">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Video *</label>
                                    <textarea class="form-control" name="video"><?php  echo $link ['link']?></textarea>
                                    <?php echo !empty($errors['vidEmpty']) ? $errors['vidEmpty'] : NULL; ?>
                                </div>
                            </div><!-- /.box-body -->

                            <div class="box-footer">

                                <button type="submit" class="btn btn-success" style="width:100px" name="submit"> Submit
                                </button>
                            </div>
                        </form>
                            <?php }else{
                            header("Location: video_list.php");
                        } ?>
                    </div><!-- /.box -->

                    <!-- Input addon -->
                    <!-- /.box -->

                </div><!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
    </aside>
    <!-- /.right-side -->
<?php include 'footer.php';?>