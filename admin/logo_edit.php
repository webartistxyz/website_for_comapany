<?php include 'header.php';?>
<?php
use App\Logo\Logo;
$obj = new Logo();
?>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->           
				<style>
				.active span{ color:red}
				 .head{font-size:21px; color:#09F;}
				 .spanhead{margin-left:20px}
				 .treeview-menu > li > a:hover{ color:#0099FF !important}
				 .arrowhead{margin-top:10px}
				</style>
				<?php include 'menu.php';?>
			</section>                <!-- /.sidebar -->
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		  <aside class="right-side" >
			<!-- Content Header (Page header) -->
			<!-- Main content -->
			<section class="content">

				<!-- Small boxes (Stat box) -->
				<div class="row">
		   
					<!-- ./col -->
				</div><!-- /.row -->

				<!-- top row -->
				<div class="row">
		  
					<!-- left column -->
						<div class="col-md-12">
							<!-- general form elements -->
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Update Record</h3>
                                </div><!-- /.box-header -->
                                <?php
                                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                    if (isset($_POST["logo"])) {
                                        $file = $_FILES;
                                        $permited  = array('jpg', 'jpeg', 'png', 'gif');
                                        $file_name = $file['image']['name'];
                                        $file_size = $file['image']['size'];
                                        $file_temp = $file['image']['tmp_name'];

                                        $div = explode('.', $file_name);
                                        $file_ext = strtolower(end($div));
                                        $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
                                        $uploaded_image = "photos/".$unique_image;
                                        empty($file_name) ? $errors['imageEmpty'] = "<span style='color: #ac2925'>Please select a logo!</span>":NULL;

                                        if ($file_size >1048567) {
                                            $errors['imageSize'] = "<span style='color: #ac2925'>Image Size should be less then 1MB!</span>";
                                        }

                                        if (!empty($file_name) && in_array($file_ext, $permited) === false) {
                                            $errors['imageFormat'] = "<span style='color: #ac2925'>You can upload only:-".implode(', ', $permited)."</span>";
                                        } else{
                                            move_uploaded_file($file_temp, $uploaded_image);
                                            $id = $_GET["id"];
                                                empty($errors) ?  $obj->updateLogo($id, $uploaded_image) : NULL;
                                        }
                                    }
                                }

                                ?>
                                <?php
                                isset($_GET["id"]) ? $id = $_GET["id"] : header("Location: logo.php");
                                $info = $obj->getLogoById($id);
                                ?>
<form action="" method="post" enctype="multipart/form-data">
	<div class="box-body">                                        
	 
	 
		<div class="form-group">
			<img src="<?php echo $info['img'];?>" alt="" width="200" height=""120"/>
		</div>
		<div class="form-group">
			<label>Change Logo Image <small> </small></label>
			<input type="file" name="image"/>
            <?php echo !empty($errors['imageEmpty']) ? $errors['imageEmpty'] : NULL; ?>
		</div>
 
	</div><!-- /.box-body -->

	<div class="box-footer">						   
		
	<button type="submit" name="logo" class="btn btn-primary" style="width:100px"> Update </button>
	</div>
</form>

							</div><!-- /.box -->							
							<!-- Input addon -->
							<!-- /.box -->		
						</div><!--/.col (left) -->
				</div>             
				<!-- /.row -->
			</section><!-- /.content -->
		</aside>
		<!-- /.right-side -->
<?php include 'footer.php';?>