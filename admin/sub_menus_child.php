<?php include 'header.php';?>
<?php
use App\Database\Database;
use App\Menu\Menu;
use App\SubMenu\SubMenu;
$menuObj = new SubMenu();
$errors = array();
?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <style>
                .active span{ color:red}
                .head{font-size:21px; color:#09F;}
                .spanhead{margin-left:20px}
                .treeview-menu > li > a:hover{ color:#0099FF !important}
                .arrowhead{margin-top:10px}
            </style>
            <?php include 'menu.php';?>
        </section>                <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side" >
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
            </div><!-- /.row -->

            <!-- top row -->
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Add New Record</h3>
                        </div><!-- /.box-header -->
                        <?php
                        /*if(isset($_POST['slider']))
                        {
                        $title=$_POST['title'];
                        $description=$_POST['description'];
                        $name=$_FILES['image']['name'];
                         $ext=(pathinfo($name,PATHINFO_EXTENSION));
                        if($ext=='jpg' or $ext=='png' or $ext=='PNG' or $ext=='jpeg' or $ext=='JPG' or $ext=='JPEG' or $ext=='GIF' or $ext=='gif')
                        {
                        $location="photos/".time().".".$ext;
                        move_uploaded_file($_FILES["image"]["tmp_name"],"../$location");
                        $sql = "INSERT INTO slider (location,description,title) VALUES ('$location', '$description','$title')";

                        if ($conn->query($sql) === TRUE) {
                                echo"<script>location.href='slider.php?message=success'</script>";
                        }
                        else {
                            echo"<script>location.href='slider.php?message=error'</script>";
                        }
                         }
                         else
                         {
                                echo"<script>location.href='slider.php?message=image_error'</script>";

                         }
                        $conn->close();
                        }*/

                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            if (isset($_POST["submit"])) {

                                empty($_POST["menu"]) ? $errors['menuEmpty'] = "<span style='color: #ac2925'>Field must not be empty!</span>":NULL;
                                empty($_POST["menuId"]) ? $errors['menuIdEmpty'] = "<span style='color: #ac2925'>Please select a menu!</span>":NULL;
                                echo empty($errors) ?  $menuObj->subMenuInsert($_POST) : NULL;


                            }
                        }
                        ?>
                        <!-- form start -->
                        <form role="form" name="entryform" id="entryform" action="" method="POST" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Menu List *</label>
                                    <br>
                                    <select name="menuId" class="form-control">
                                        <option>&larr; Select a Sub-Menu &rarr;</option>
                                        <?php
                                        $allMenu = SubMenu::getAllSubMenu();
                                        if (isset($allMenu)){
                                            foreach ($allMenu as $menu){

                                                ?>
                                                <option value="<?= $menu["id"]; ?>"><?= $menu["name"]; ?></option>
                                                <?php
                                            }}
                                        ?>
                                    </select>
                                    <?php echo !empty($errors['menuIdEmpty']) ? $errors['menuIdEmpty'] : NULL; ?>
                                </div>
                                <div class="form-group">
                                    <label>Add Sub-Menu's Child Name*</label>
                                    <input class="form-control" name="menu" required/>
                                    <?php echo !empty($errors['menuEmpty']) ? $errors['menuEmpty'] : NULL; ?>
                                </div>
                                <div class="form-group">
                                    <label>Add Description</label>
                                    <textarea class="form-control tinymce" name="link"></textarea>
                                </div>
                            </div><!-- /.box-body -->

                            <div class="box-footer">

                                <button type="submit" name="submit" class="btn btn-success" style="width:100px"> Submit
                                </button>
                            </div>
                        </form>

                    </div><!-- /.box -->

                    <!-- Input addon -->
                    <!-- /.box -->

                </div><!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
    </aside>
    <!-- /.right-side -->
<?php include 'footer.php';?>