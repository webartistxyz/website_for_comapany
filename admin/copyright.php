<?php include 'header.php';?>
<?php
use App\Database\Database;
use App\Session\Session;
use App\CopyRight\CopyRight;
?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <style>
                .active span{ color:red}
                .head{font-size:21px; color:#09F;}
                .spanhead{margin-left:20px}
                .treeview-menu > li > a:hover{ color:#0099FF !important}
                .arrowhead{margin-top:10px}
            </style>
            <?php include 'menu.php';?>
        </section>                <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side" >
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
            </div><!-- /.row -->

            <!-- top row -->
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <?php
                        echo Session::SuccessMsg();
                        echo Session::ErrorMsg();
                        ?>
                        <?php

                        $sql = "SELECT * FROM copyright";
                        $stmt = Database::Prepare($sql);
                        $stmt->execute();
                        $info = $stmt->fetch();

                        echo "<table class=\"table table-bordered\">";
                        echo "<tr class=\"success\">
		
                            <th>Copy Right Text</th>
                            <th>Action</th>";

                            ?>
                            <tr class="warning">
                                <td><?php echo $info['title'];?></td>

                                <td>
					<span class="pull-right">
					<a class="btn btn-primary" href="copyright_edit.php?id=<?php echo $info['id'];?>" title="Edit"><i class="fa
					fa-pencil-square-o"></i></a>
					</span>
                                </td>
                            </tr>
                        </table>


                    </div><!-- /.box -->

                    <!-- Input addon -->
                    <!-- /.box -->

                </div><!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
    </aside>
    <!-- /.right-side -->
<?php include 'footer.php';?>