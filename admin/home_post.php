<?php include 'header.php';?>
<?php
use App\Home\Home;
$home = new Home();
$errors = array();
?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <style>
                .active span{ color:red}
                .head{font-size:21px; color:#09F;}
                .spanhead{margin-left:20px}
                .treeview-menu > li > a:hover{ color:#0099FF !important}
                .arrowhead{margin-top:10px}
            </style>
            <?php include 'menu.php';?>
        </section>                <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side" >
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
            </div><!-- /.row -->

            <!-- top row -->
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Add New Record</h3>
                        </div><!-- /.box-header -->
                        <?php
                        /*if(isset($_POST['slider']))
                        {
                        $title=$_POST['title'];
                        $description=$_POST['description'];
                        $name=$_FILES['image']['name'];
                         $ext=(pathinfo($name,PATHINFO_EXTENSION));
                        if($ext=='jpg' or $ext=='png' or $ext=='PNG' or $ext=='jpeg' or $ext=='JPG' or $ext=='JPEG' or $ext=='GIF' or $ext=='gif')
                        {
                        $location="photos/".time().".".$ext;
                        move_uploaded_file($_FILES["image"]["tmp_name"],"../$location");
                        $sql = "INSERT INTO slider (location,description,title) VALUES ('$location', '$description','$title')";

                        if ($conn->query($sql) === TRUE) {
                                echo"<script>location.href='slider.php?message=success'</script>";
                        }
                        else {
                            echo"<script>location.href='slider.php?message=error'</script>";
                        }
                         }
                         else
                         {
                                echo"<script>location.href='slider.php?message=image_error'</script>";

                         }
                        $conn->close();
                        }*/

                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            if (isset($_POST["submit"])) {

                                empty($_POST["title"]) ? $errors['titleEmpty'] = "<span style='color: #ac2925'>Title must not be empty!</span>":NULL;

                                empty($_POST["description"]) ? $errors['descriptionEmpty'] = "<span style='color: #ac2925'>Description must not be empty!</span>":NULL;

                                empty($_POST["link"]) ? $errors['linkEmpty'] = "<span style='color: #ac2925'>Slider link must not be empty!</span>":NULL;

                                $file = $_FILES;
                                $permited  = array('jpg', 'jpeg', 'png', 'gif');
                                $file_name = $file['image']['name'];
                                $file_size = $file['image']['size'];
                                $file_temp = $file['image']['tmp_name'];

                                $div = explode('.', $file_name);
                                $file_ext = strtolower(end($div));
                                $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
                                $uploaded_image = "photos/".$unique_image;

                                empty($file_name) ? $errors['imageEmpty'] = "<span style='color: #ac2925'>Please select a slider image!</span>":NULL;

                                if ($file_size >1048567) {
                                    $errors['imageSize'] = "<span style='color: #ac2925'>Image Size should be less then 1MB!</span>";
                                }

                                if (in_array($file_ext, $permited) === false) {
                                    $errors['imageFormat'] = "<span style='color: #ac2925'>You can upload only:-".implode(', ', $permited)."</span>";
                                } else{
                                    move_uploaded_file($file_temp, $uploaded_image);

                                    echo empty($errors) ?  $home->HomeInsert($_POST, $uploaded_image) : NULL;

                                }
                            }
                        }




                        ?>
                        <!-- form start -->
                        <form role="form" name="entryform" id="entryform" action="" method="POST" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Title *</label>
                                    <input class="form-control" name="title" />
                                    <?php echo !empty($errors['titleEmpty']) ? $errors['titleEmpty'] : NULL; ?>
                                </div>
                                <div class="form-group">
                                    <label>Description *</label>
                                    <textarea class="form-control tinymce" name="description" ></textarea>
                                    <?php echo !empty($errors['descriptionEmpty']) ? $errors['descriptionEmpty'] : NULL; ?>
                                </div>
                                <div class="form-group">
                                    <label>Link *</label>
                                    <input class="form-control" name="link" />
                                    <?php echo !empty($errors['linkEmpty']) ? $errors['linkEmpty'] : NULL; ?>
                                </div>
                                <div class="form-group">
                                    <label>Slider Image * <small> (Image size Must be width: 1920px & Height: 1240px)</small></label>
                                    <input type="file" name="image" />
                                    <?php echo !empty($errors['imageEmpty']) ? $errors['imageEmpty'] : NULL; ?>
                                    <?php echo !empty($errors['imageSize']) ? $errors['imageSize'] : NULL; ?>
                                    <?php echo !empty($errors['imageFormat']) ? $errors['imageFormat'] : NULL; ?>
                                </div>

                            </div><!-- /.box-body -->

                            <div class="box-footer">

                                <button type="submit" name="submit" class="btn btn-success" style="width:100px"> Submit
                                </button>
                            </div>
                        </form>

                    </div><!-- /.box -->

                    <!-- Input addon -->
                    <!-- /.box -->

                </div><!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
    </aside>
    <!-- /.right-side -->
<?php include 'footer.php';?>