<?php include "../vendor/autoload.php" ?>
<?php
use App\AdminLogin\AdminLogin;
$admin_login = new AdminLogin();
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	$userName = $_POST["username"];
	$password = md5(md5($_POST["password"]));
	$loginCheck = $admin_login->admin_login($userName, $password);
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Admin panel</title>
<link href=" bootstrap.min.css" rel="stylesheet" type="text/css">
<link href=" style.css" rel="stylesheet" type="text/css">
<link href="css/login.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="main">
		<div id="login">
			<h2>Admin Login</h2>
			<form action="" method="post">
				<label>UserName :</label>
				<input id="name" name="username" placeholder="username" type="text">
				<label>Password :</label>
				<input id="password" name="password" placeholder="**********" type="password">
				<input class="submit" name="submit" type="submit" value=" Login "> 
				<span>
                    <?php
                    if (isset($loginCheck)){
	                    echo $loginCheck;
                    }
                    ?>
                </span>
			</form>
		</div>
	</div>
</body>
</html>