<?php include 'header.php';?>
<?php
use App\Database\Database;
use App\Session\Session;
use App\SubMenu\SubMenu;
use App\Helpers\Helpers;
?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <style>
                .active span{ color:red}
                .head{font-size:21px; color:#09F;}
                .spanhead{margin-left:20px}
                .treeview-menu > li > a:hover{ color:#0099FF !important}
                .arrowhead{margin-top:10px}
            </style>
            <?php include 'menu.php';?>
        </section>                <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side" >
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="row">

                <!-- ./col -->
            </div><!-- /.row -->

            <!-- top row -->
            <div class="row">

                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <br>
                        <div class="box-header text-center">
                            <h1 class="box-title">Page List</h1>
                            <a class='btn btn-success pull-right' href="sub_menu_add.php">Add New</a>
                        </div>
                        <?php
                        echo Session::SuccessMsg();
                        echo Session::ErrorMsg();
                        ?>
                        <?php
                        if (isset($_GET["id"])){
                            $delId = $_GET["id"];
                            echo SubMenu::deleteMenu($delId);
                        }
                        ?>
                        <?php
                        $sql = "SELECT * FROM sub_menu order by id DESC";
                        $stmt = Database::Prepare($sql);
                        $stmt->execute();
                        $result = $stmt->fetchAll();
                        ?>

                        <table id="table_info" class="table table-bordered table-striped">
                            <thead>
                            <tr class = "success">
                                <th>No.</th>
                                <th>Title</th>
                                <th width="50%">Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                        <?php
                        $x = 0;
                        foreach ($result as $info)
                        {
                            $x++;
                            ?>
                            <tr class="warning">
                                <td><?php echo $x;?></td>
                                <td><?php echo $info['name'];?></td>
                                <td><?php echo Helpers::textShorten($info['link'], 200);?></td>
                                <td>
					<span class="pull-right">
					<a class="btn btn-primary" href="sub_menu_edit.php?id=<?php echo $info['id'];?>" title="Edit"><i
                                class="fa
					fa-pencil-square-o"></i></a>
					<a class="btn btn-danger" href="?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" title="Delete"><i class="fa fa-times-circle"></i></a>
					</span>
                                </td>
                            </tr>

                        <?php }?>
                            </tbody>
                        </table>
                    </div><!-- /.box -->

                    <!-- Input addon -->
                    <!-- /.box -->

                </div><!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
    </aside>
    <!-- /.right-side -->
    <script>
        $(document).ready(function(){
            $('#table_info').DataTable();
        });
    </script>
<?php include 'footer.php';?>