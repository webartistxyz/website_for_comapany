<?php include "include/header.php"; ?>
<?php
use App\Team\Team;
isset($_GET["id"]) ? $id = $_GET["id"] : header("Location: team.php");
$info = Team::geTeamById($id);
?>
<div class="top_panel_title_wrap">
    <div class="content_wrap">
        <div class="top_panel_title">
            <div class="page_title">
                <div class="post_meta"></div>
                <h3 class="page_caption">Team Member</h3>
            </div>
            <div class="breadcrumbs">
                <a class="breadcrumbs_item home" href="index.php">Home</a>
                <span class="breadcrumbs_delimiter"></span>
                <a href="team.php">All Team Members</a>
                <span class="breadcrumbs_delimiter"></span>
                <span class="breadcrumbs_item current">Team Member</span>
            </div>
        </div>
    </div>
</div>
<br><br><br>
<?php
if (isset($info)){
?>
            <div class="page_content_wrap scheme_default">
                <div class="content_wrap">
                    <div class="content">
                        <article class="team_member_page cpt_team type-cpt_team has-post-thumbnail hentry">
                            <div class="team_member_content entry-content">
                                <div class="vc_row wpb_row vc_row-fluid fullwidth_1">
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_single_image wpb_content_element vc_align_left">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper vc_box_border_grey">
                                                            <img src="admin/<?= $info['location']; ?>"
                                                                 class="vc_single_image-img
                                                            attachment-full" alt="<?= $info['name']; ?>" />
                                                        </div>
                                                    </figure>
                                                </div>
                                                <div class="vc_empty_space empty_3">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-8">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper uni1">
                                                        <h5><?= $info['designation']; ?></h5>
                                                        <h2><?= $info['name']; ?></h2>
	                                                    <p><?= $info['description']; ?></p>

                                                    </div>
                                                </div>
                                                <div class="vc_empty_space empty_3">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_row-full-width vc_clearfix"></div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
<?php } ?>
          <?php include "include/footer.php";?>