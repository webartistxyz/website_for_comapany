<?php ob_start(); ?>
<?php include "vendor/autoload.php"; ?>
<?php
use App\Menu\Menu;
use App\SubMenu\SubMenu;
use App\Logo\Logo;
use App\Address\Address;
use App\Social\Social;
use App\CopyRight\CopyRight;
use App\Team\Team;
use App\Portfolio\Portfolio;
$fixedTitle = CopyRight::getTitle();
$social = new Social();
$address = new Address();
$addressInfo = $address->getAllAddress();
?>


<!DOCTYPE html>
<html lang="en-US" class="no-js scheme_default">
<head>
	<?php
	$path = $_SERVER["SCRIPT_FILENAME"];
	$currentPage = basename($path, '.php');
	if (isset($_GET["pageId"])){
		$id = $_GET["pageId"];
		$title = Menu::getMenuById($id);
		if ($title){
			?>
            <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
			<?php
		}
	}elseif (isset($_GET["subPageId"])){
		$subId = $_GET["subPageId"];
		$title = SubMenu::getMenuByIdForEdit($subId);
		if ($title) {
			?>
            <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
			<?php
		} }
    elseif (isset($_GET["childId"])){
		$childId = $_GET["childId"];
		$title = SubMenu::getChildMenuByIdForEdit($childId);
		if ($title) {
			?>
            <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
			<?php
		} }
    elseif (isset($_GET["id"])){
		$id = $_GET["id"];
		$title = Team::geTeamById($id);
		if ($title) {
			?>
            <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
			<?php
		} }
    elseif (isset($_GET["portId"])){
		$id = $_GET["portId"];
		$title = Portfolio::getPortfolioById($id);
		if ($title) {
			?>
            <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
			<?php
		} }
    elseif($currentPage == 'team'){
		?>
        <title><?php  echo  "Our Team"; ?> | <?php  echo $fixedTitle['title']; ?></title>
		<?php
	}
    elseif($currentPage == 'portfolio'){
		?>
        <title><?php  echo  "Our Activities"; ?> | <?php  echo $fixedTitle['title']; ?></title>
		<?php
	}
	else{
		?>
        <title><?php  echo  "Home"; ?> | <?php  echo $fixedTitle['title']; ?></title>
		<?php
	}
	?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no" />
	<link rel='stylesheet' href='js/custom/trx/trx_addons_icons-embedded.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/vendor/swiper/swiper.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/custom/trx/trx_addons.css' type='text/css' media='all' />
	<link rel='stylesheet' href='fonts/Carnas/stylesheet.css' type='text/css' media='all' />
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C800italic%2C300%2C300italic%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800&amp;subset=latin%2Clatin-ext&amp;ver=4.7.3' type='text/css' media='all' />
	<link rel='stylesheet' href='css/fontello/css/fontello.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/animation.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/colors.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/styles.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/vendor/mediaelement/mediaelementplayer.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/vendor/mediaelement/mediaelement.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />

	<link rel="icon" href="images/cropped-fav-big-32x32.jpg" sizes="32x32" />
	<link rel="icon" href="images/cropped-fav-big-192x192.jpg" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="images/cropped-fav-big-180x180.jpg" />
	<meta name="msapplication-TileImage" content="images/cropped-fav-big-270x270.jpg" />

</head>

<body class="pformats archive body_style_wide scheme_default blog_mode_blog blog_style_excerpt sidebar_show sidebar_right header_style_header-2 header_title_on no_layout vc_responsive">
<div class="body_wrap">
	<div class="page_wrap">
		<header class="top_panel top_panel_style_2 scheme_default">
			<div class="top_panel_fixed_wrap"></div>
			<div class="top_panel_navi scheme_dark">
				<div class="menu_main_wrap clearfix">
					<div class="content_wrap">
						<a class="logo scheme_dark" href="index.php">
							<?php $logo = Logo::getLogo(); ?>
							<img src="admin/<?= $logo['img']; ?>" class="logo_main" alt="">
						</a>
						<nav class="menu_main_nav_area menu_hover_fade">
							<div class="menu_main_inner">
								<div class="contact_wrap scheme_default ">
                                    <div class="phone_wrap icon-mobile"><?php echo $addressInfo['mobile']?></div>
                                    <div class="socials_wrap">
										<?php $link = $social->getAllSocial(); if (isset($link)){ ?>
                                            <span class="social_item">
                                            <a href="<?= $link['facebook'] ?>" target="_blank" class="social_icons
                                            social_facebook">
                                                <span class="trx_addons_icon-facebook"></span>
                                            </a>
                                        </span>
                                            <span class="social_item">
                                            <a href="<?= $link['twitter'] ?>" target="_blank" class="social_icons social_twitter">
                                                <span class="trx_addons_icon-twitter"></span>
                                            </a>
                                        </span>
                                            <span class="social_item">
                                            <a href="<?= $link['youtube'] ?>" target="_blank" class="social_icons social_youtube">
                                                <span class="trx_addons_icon-youtube"></span>
                                            </a>
                                        </span>
										<?php } ?>
                                    </div>
									<div class="search_wrap search_style_fullscreen">
										<div class="search_form_wrap">
											<form role="search" method="get" class="search_form" action="#">
												<input type="text" class="search_field" placeholder="Search" value="" name="s">
												<button type="submit" class="search_submit icon-search"></button>
												<a class="search_close icon-cancel"></a>
											</form>
										</div>
										<div class="search_results widget_area">
											<a href="#" class="search_results_close icon-cancel"></a>
											<div class="search_results_content"></div>
										</div>
									</div>
								</div>
								<ul id="menu_main" class="sc_layouts_menu_nav menu_main_nav">
									<?php
									$allMenu = Menu::getAllMenu();
									if (isset($allMenu)){
									    $x = 0;
										foreach ($allMenu as $menu) {
										    $x++;
											$allSubMenu = SubMenu::getMenuById($menu['id']);
											?>
											<li class="menu-item <?php echo !empty($allSubMenu) ? 'menu-item-has-children':NULL ?>
                                            <?php
											if (strtolower($menu['name']) == 'home'){echo 'current-menu-ancestor';} ?>">
												<a href="main_details.php?pageId=<?= urlencode($menu['id']); ?>"><span><?=
                                                        $menu['name']; ?></span></a>
												<?php
												if (!empty($allSubMenu)){
													?>
													<ul class="sub-menu">
														<?php
														foreach ($allSubMenu as $sub) {
															$allChildMenu = SubMenu::getChildById($sub['id']);
															?>
															<li class="menu-item <?php echo !empty($allChildMenu) ? 'menu-item-has-children':NULL ?>">
																<a href="sub_details.php?subPageId=<?= urlencode($sub['id']); ?>">
                                                                    <span><?= $sub['name'];
																		?></span></a>
																<?php

																if (!empty($allChildMenu)) {
																	?>
																	<ul class="sub-menu">
																		<?php
																		foreach ($allChildMenu as $child) {
																			?>
																			<li class="menu-item">
																				<a href="chaild_details.php?childId=<?=
                                                                                urlencode($child['id']); ?>"><span><?= $child['name']; ?></span></a>
																			</li>
																		<?php } ?>
																	</ul>
																<?php } ?>
															</li>
														<?php }
														if ($x <= 1){
															?>
                                                            <li class="menu-item">
                                                                <a href="team.php">
                                                                    <span>Our Team</span>
                                                                </a>
                                                            </li>
														<?php } if ($x == 2){?>
                                                            <li class="menu-item">
                                                                <a href="portfolio.php">
                                                                    <span>Our Activities</span>
                                                                </a>
                                                            </li>
														<?php } ?>
													</ul>
												<?php } ?>
											</li>
											<?php
										}}
									?>
								</ul>
							</div>
						</nav>
						<a class="menu_mobile_button"></a>
					</div>
				</div>
			</div>
		</header>
		<div class="menu_mobile_overlay"></div>
		<div class="menu_mobile scheme_">
			<div class="menu_mobile_inner">
				<a class="menu_mobile_close icon-cancel"></a>
				<nav class="menu_mobile_nav_area">
					<ul id="menu_mobile" class="sc_layouts_menu_nav menu_main_nav">
						<?php
						$allMenu = Menu::getAllMenu();
						if (isset($allMenu)){
						    $x = 0;
							foreach ($allMenu as $menu) {
							    $x++;
								$allSubMenu = SubMenu::getMenuById($menu['id']);
								?>
								<li class="menu-item <?php echo !empty($allSubMenu) ? 'menu-item-has-children':NULL ?>
                                <?php
								if (strtolower($menu['name']) == 'home'){echo 'current-menu-ancestor';} ?>">
									<a href="main_details.php?pageId=<?= urlencode($menu['id']); ?>">
                                        <span><?= $menu['name']; ?></span></a>
									<?php
									if (!empty($allSubMenu)){
										?>
										<ul class="sub-menu">
											<?php
											foreach ($allSubMenu as $sub) {
												$allChildMenu = SubMenu::getChildById($sub['id']);
												?>
												<li class="menu-item <?php echo !empty($allChildMenu) ? 'menu-item-has-children':NULL ?>">
													<a href="sub_details.php?subPageId=<?= urlencode($sub['id']); ?>">
                                                        <span><?= $sub['name'];
															?></span></a>
													<?php

													if (!empty($allChildMenu)) {
														?>
														<ul class="sub-menu">
															<?php
															foreach ($allChildMenu as $child) {
																?>
																<li class="menu-item">
																	<a href="child_details.php?childId=<?= urlencode($child['id']); ?>">
                                                                        <span><?= $child['name']; ?></span></a>
																</li>
															<?php } ?>
														</ul>
													<?php } ?>
												</li>
											<?php }
											if ($x <= 1){
												?>
                                                <li class="menu-item">
                                                    <a href="team.php">
                                                        <span>Our Team</span>
                                                    </a>
                                                </li>
											<?php } if ($x == 2){?>
                                                <li class="menu-item">
                                                    <a href="portfolio.php">
                                                        <span>Our Activities</span>
                                                    </a>
                                                </li>
											<?php } ?>
										</ul>
									<?php } ?>
								</li>
								<?php
							}}
						?>
					</ul>
				</nav>
				<div class="search_mobile">
					<div class="search_form_wrap">
						<form role="search" method="get" class="search_form" action="#">
							<input type="text" class="search_field" placeholder="Search ..." value="" name="s">
							<button type="submit" class="search_submit icon-search" title="Start search"></button>
						</form>
					</div>
				</div>
                <div class="socials_mobile">
                    <span class="social_item">
                        <a href="<?= $link['facebook'] ?>" target="_blank" class="social_icons social_facebook">
                            <span class="trx_addons_icon-facebook"></span>
                        </a>
                    </span>
                    <span class="social_item">
                        <a href="<?= $link['twitter'] ?>" target="_blank" class="social_icons social_twitter">
                            <span class="trx_addons_icon-twitter"></span>
                        </a>
                    </span>
                    <span class="social_item">
                        <a href="<?= $link['youtube'] ?>" target="_blank" class="social_icons social_yutube">
                            <span class="trx_addons_icon-gplus"></span>
                        </a>
                    </span>
                </div>
			</div>
		</div>