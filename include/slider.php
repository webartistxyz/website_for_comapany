<?php
use App\Slider\Slider;
?>
<div class="page_content_wrap scheme_default">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item_single page hentry">
                <div class="post_content entry-content">
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid bg_gradient vc_custom_1467796921857 vc_row-has-fill">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery">
                                        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" data-version="5.3.1.5">
                                            <ul>
                                                <?php
                                                $allSlider = Slider::getAllSlider();
                                                if (isset($allSlider)){
                                                    $i = 7;
                                                    foreach ($allSlider as $slider){
                                                        $i++;
                                                ?>
                                                <li data-index="rs-<?php echo $i; ?>" data-transition="fade"
                                                    data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="images/1-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                    <img src="admin/<?php echo $slider['location']; ?>" alt="" title="8"
                                                         width="1170"
                                                         height="593" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-1-layer-4" data-x="['left','left','left','left']" data-hoffset="['-10','-10','-10','-10']" data-y="['top','top','top','top']" data-voffset="['-10','-10','-10','-10']" data-width="500" data-height="1000" data-whitespace="nowrap" data-visibility="['off','off','off','on']" data-type="shape" data-responsive_offset="on" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":300,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"> </div>
                                                    <div class="tp-caption tp-resizeme" id="slide-<?php echo $i; ?>-layer-1"
                                                         data-x="['left','left','left','left']" data-hoffset="['120','40','60','30']" data-y="['top','top','top','top']" data-voffset="['150','150','150','150']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:top;","speed":700,"to":"o:1;","delay":1200,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"><?php echo $slider['title']; ?> </div>
                                                    <div class="tp-caption tp-resizeme" id="slide-<?php echo $i; ?>-layer-2"
                                                         data-x="['left','left','left','left']" data-hoffset="['120','40','60','30']" data-y="['top','top','top','top']" data-voffset="['180','180','180','180']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":700,"to":"o:1;","delay":700,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"><?php echo wordwrap(ucwords($slider['description']),16,"<br>\n"); ?> </div>
                                                    <div class="tp-caption tp-resizeme" id="slide-<?php echo $i; ?>-layer-3"
                                                         data-x="['left','left','left','left']" data-hoffset="['120','40','60','30']" data-y="['top','top','top','top']" data-voffset="['357','357','357','357']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-actions='[{"event":"click","action":"simplelink","target":"_blank","url":"https:\/\/<?php echo $slider['link'];
                                                         ?>","delay":""}]' data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":700,"to":"o:1;","delay":1700,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;background:linear-gradient(to right,#b9d057 0%,#b9d057 100%);"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[50,50,50,50]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]">More info </div>
                                                </li>
                                                <?php }} ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>