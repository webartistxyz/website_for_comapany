<footer class="site_footer_wrap">
    <div class="footer_wrap widget_area scheme_dark">
        <div class="footer_wrap_inner widget_area_inner">
            <div class="content_wrap">
                <div class="columns_wrap">
                    <aside class="column-1_4 widget widget_text">
                        <div class="textwidget">
                            <img src="admin/<?= $logo['img']; ?>" alt="HR Advisor">
                            <ul class="trx_addons_list  icons">
                                <li class="icon-home-alt">
	                                <?php echo wordwrap(ucwords($addressInfo['location']),28,"<br>\n"); ?>
                                </li>
                                <li class="icon-white"><?php echo $addressInfo['email']?></li>
                                <li class="icon-tablet"><?php echo $addressInfo['mobile']?></li>
                            </ul>
                        </div>
                    </aside>
                    <?php
                    use App\SubMenu\SubMenu;
                    if (isset($allMenu)){
                    $x = 0;
                    foreach ($allMenu as $menu) {
	                    $x ++;
	                    if ($x <= 3){
		                    $allSubMenu = SubMenu::getMenuById($menu['id']);
	                    ?>
                        <aside class="column-1_4 widget widget_nav_menu">
                            <h5 class="widget_title"><?= $menu['name']; ?></h5>
                            <div class="menu-footer-links-container">
                                <ul id="menu-footer-links" class="menu">
                                <?php
                                if (!empty($allSubMenu)){
	                                foreach ($allSubMenu as $sub) {
			                    ?>
                                    <li class="menu-item"><a href="sub_details.php?id=<?= urlencode($sub['id']); ?>"><?= $sub['name']; ?></a></li>
                                <?php }} ?>
                                <?php
                                if ($x <= 1){
                                ?>
                                <li class="menu-item">
                                    <a href="team.php">
                                        <span>Our Team</span>
                                    </a>
                                </li>
                                <?php } if ($x == 2){?>
                                    <li class="menu-item">
                                        <a href="portfolio.php">
                                            <span>Our Activities</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                </ul>
                            </div>
                        </aside>
	                    <?php }}} ?>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright_wrap scheme_dark">
        <div class="copyright_wrap_inner">
            <div class="content_wrap">
                <div class="copyright_text">
                    <div class="columns_wrap">
                        <?php
                        use App\CopyRight\CopyRight;
                        $info = CopyRight::getText();
                        ?>
                        <div class="column-1_2"><?= $info['title']; ?> &copy;<?php echo date('Y'); ?>. All rights
                            reserved
                            .</div>
                        <div class="column-1_2">
                            <a href="#">Terms of use</a> and
                            <a href="#">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</div>

<script type='text/javascript' src='js/vendor/jQuery/jquery.js'></script>
<script type='text/javascript' src='js/vendor/jQuery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='js/custom/custom.js'></script>
<script type='text/javascript' src='js/vendor/revslider/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='js/vendor/revslider/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript' src='js/vendor/revslider/extensions/revolution.extension.slideanims.min.js'></script>
<script type='text/javascript' src='js/vendor/revslider/extensions/revolution.extension.actions.min.js'></script>
<script type='text/javascript' src='js/vendor/revslider/extensions/revolution.extension.layeranimation.min.js'></script>
<script type='text/javascript' src='js/vendor/revslider/extensions/revolution.extension.navigation.min.js'></script>
<script type='text/javascript' src='js/vendor/swiper/swiper.jquery.min.js'></script>
<script type='text/javascript' src='js/custom/trx/trx_addons.js'></script>
<script type='text/javascript' src='js/custom/scripts.js'></script>
<script type='text/javascript' src='js/custom/embed.min.js'></script>
<script type='text/javascript' src='js/vendor/js_comp/js_comp.min.js'></script>
<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>
</html>
<?php ob_end_flush(); ?>