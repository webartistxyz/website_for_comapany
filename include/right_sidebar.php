<?php
use App\Video\Video;
$video = new Video();
?>

<div class="sidebar right widget_area scheme_default" role="complementary">
    <div class="sidebar_inner">


        <aside class="widget widget_recent_comments">

            <h5 class="widget_title">Quick Contact</h5>
            <?php echo $addressInfo['address']?>
        </aside>
        <aside class="widget widget_recent_comments">
            <?php $link = $video->frontVideo(); ?>
            <h5 class="widget_title">Featured Video</h5>
            <?php echo $link["link"]; ?>
        </aside>
    </div>
</div>
