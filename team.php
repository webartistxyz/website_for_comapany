<?php include "include/header.php";?>
<?php
use App\Team\Team;
?>
<div class="top_panel_title_wrap">
    <div class="content_wrap">
        <div class="top_panel_title">
            <div class="page_title">
                <h3 class="page_caption">Our Team</h3>
            </div>
            <div class="breadcrumbs">
                <a class="breadcrumbs_item home" href="index.php">Home</a>
                <span class="breadcrumbs_delimiter"></span>
                <span class="breadcrumbs_item current">Our Team</span>
            </div>
        </div>
    </div>
</div>
<br><br><br>
            <div class="page_content_wrap scheme_default">
                <div class="content_wrap">
                    <div class="content">
                        <article class="post_item_single page hentry">
                            <div class="post_content entry-content">
                                <div class="vc_row wpb_row vc_row-fluid fullwidth_1">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">

                                        </div>
                                    </div>
                                </div>
                                <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1468167409309 vc_row-has-fill fullwidth_1">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper uniq">
                                                        <h2 class="sc_team_title sc_item_title">Our Professionals</h2>
                                                    </div>
                                                </div>
                                                <div class="sc_team sc_team_default" data-slides-per-view="3" data-slides-min-width="150">
                                                    <div class="sc_team_columns sc_item_columns trx_addons_columns_wrap columns_padding_bottom">
                                                        <?php
                                                        $allInfo = Team::getAllTeam();
                                                        if (isset($allInfo)){
                                                            foreach ($allInfo as $info){
                                                        ?>
                                                        <div class="trx_addons_column-1_3">
                                                            <div class="sc_team_item">
                                                                <div class="sc_team_item_avatar trx_addons_hover trx_addons_hover_style_zoomin">
                                                                    <img src="admin/<?= $info['location'] ?>"
                                                                         alt="<?= $info['name']; ?>" />
                                                                    <div class="trx_addons_hover_mask"></div>
                                                                    <div class="trx_addons_hover_content">
                                                                        <h5 class="trx_addons_hover_title">
                                                                            <a href="single-team.php?id=<?= $info['id']; ?>">
                                                                                <?= $info['name']; ?>
                                                                            </a>
                                                                        </h5>
                                                                        <div class="trx_addons_hover_subtitle">
                                                                            <?= $info['designation']; ?>
                                                                        </div>
                                                                        <a href="single-team.php?id=<?= $info['id']; ?>"
                                                                           class="trx_addons_hover_icon trx_addons_hover_icon_link"></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php }} ?>
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space empty_6">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_row-full-width vc_clearfix"></div>
                                <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                    </div>
                                </div>
                            </div>
                        </article>

                    </div>
                </div>
            </div>
<?php include "include/footer.php";?>