<?php include "include/header.php"; ?>
<?php include "include/slider.php"; ?>
<?php
use App\Home\Home;
use App\Helpers\Helpers;
?>

                    <div class="vc_row-full-width vc_clearfix"></div>
                    <div class="vc_row wpb_row vc_row-fluid fullwidth_1">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="vc_empty_space empty_6-8">
                                        <span class="vc_empty_space_inner"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_row-fluid fullwidth_1">
                        <div class="wpb_column vc_column_container vc_col-sm-8">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="sc_courses sc_courses_default" data-slides-per-view="3"
                                         data-slides-min-width="150">
                                        <div class="sc_courses_columns sc_item_columns trx_addons_columns_wrap columns_padding_bottom">
                                            <?php
                                            $AllInfo = Home::getAllHomePost();
                                            if (isset($AllInfo)){
                                                foreach ($AllInfo as $info) {

                                            ?>
                                            <div class="trx_addons_column-1_2">
                                                <div class="sc_courses_item trx_addons_hover trx_addons_hover_style_links">
                                                    <div class="sc_courses_item_thumb">
                                                        <img src="admin/<?= $info['location']; ?>"
                                                             height="300" width="250"
                                                             class="attachment-hr_advisor-thumb-med size-hr_advisor-thumb-med"
                                                             alt=""/>
                                                        <span class="sc_courses_item_categories">
                                                    <a href="<?= $info['link']; ?>"
                                                       title="View all posts in Motivation"><?= $info['title']; ?></a>
                                                </span>
                                                    </div>
                                                    <div class="sc_courses_item_info">
                                                        <div class="sc_courses_item_header">
                                                            <h4 class="sc_courses_item_title">
                                                                <?= Helpers::textShorten
                                                                ($info['description'], 70); ?>
                                                                </h4>
                                                            <!--<div class="sc_courses_item_date">Started on July 7, 2016</div>-->
                                                        </div>
                                                    </div>
                                                    <div class="trx_addons_hover_mask"></div>
                                                    <div class="trx_addons_hover_content">
                                                        <h4 class="trx_addons_hover_title"><?= $info['title']; ?></h4>
                                                        <div class="trx_addons_hover_text">
                                                            <?= Helpers::textShorten
                                                            ($info['description'], 300); ?>&hellip;
                                                        </div>
                                                        <div class="trx_addons_hover_links">
                                                            <a href="<?= $info['link']; ?>"
                                                               class="trx_addons_hover_link">More
                                                                Info</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            }}
                                            ?>

                                        </div>
                                    </div>
                                    <div class="vc_empty_space empty_8-7">
                                        <span class="vc_empty_space_inner"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <?php include "include/right_sidebar.php" ;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>
<?php include "include/footer.php"; ?>