<?php ob_start(); ?>
<?php include "vendor/autoload.php"; ?>
<?php
use App\Menu\Menu;
use App\SubMenu\SubMenu;
use App\Logo\Logo;
use App\Address\Address;
use App\Social\Social;
use App\CopyRight\CopyRight;
use App\Team\Team;
use App\Portfolio\Portfolio;
$fixedTitle = CopyRight::getTitle();
$social = new Social();
$address = new Address();
$addressInfo = $address->getAllAddress();
?>


    <!DOCTYPE html>
    <html lang="en-US" class="no-js scheme_default">
    <head>
		<?php
		$path = $_SERVER["SCRIPT_FILENAME"];
		$currentPage = basename($path, '.php');
		if (isset($_GET["pageId"])){
			$id = $_GET["pageId"];
			$title = Menu::getMenuById($id);
			if ($title){
				?>
                <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
				<?php
			}
		}elseif (isset($_GET["subPageId"])){
			$subId = $_GET["subPageId"];
			$title = SubMenu::getMenuByIdForEdit($subId);
			if ($title) {
				?>
                <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
				<?php
			} }
        elseif (isset($_GET["childId"])){
			$childId = $_GET["childId"];
			$title = SubMenu::getChildMenuByIdForEdit($childId);
			if ($title) {
				?>
                <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
				<?php
			} }
        elseif (isset($_GET["id"])){
			$id = $_GET["id"];
			$title = Team::geTeamById($id);
			if ($title) {
				?>
                <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
				<?php
			} }
        elseif (isset($_GET["portId"])){
			$id = $_GET["portId"];
			$title = Portfolio::getPortfolioById($id);
			if ($title) {
				?>
                <title><?php  echo  $title['name']; ?> | <?php  echo $fixedTitle['title']; ?></title>
				<?php
			} }
        elseif($currentPage == 'team'){
			?>
            <title><?php  echo  "Our Team"; ?> | <?php  echo $fixedTitle['title']; ?></title>
			<?php
		}
        elseif($currentPage == 'portfolio'){
			?>
            <title><?php  echo  "Our Activities"; ?> | <?php  echo $fixedTitle['title']; ?></title>
			<?php
		}
		else{
			?>
            <title><?php  echo  "Home"; ?> | <?php  echo $fixedTitle['title']; ?></title>
			<?php
		}
		?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no" />
	<link rel='stylesheet' href='js/custom/trx/trx_addons_icons-embedded.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/vendor/magnific/magnific-popup.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/custom/trx/trx_addons.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/vendor/js_comp/js_comp_custom.css' type='text/css' media='all' />
	<link rel='stylesheet' href='fonts/Carnas/stylesheet.css' type='text/css' media='all' />
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C800italic%2C300%2C300italic%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800&amp;subset=latin%2Clatin-ext&amp;ver=4.7.3' type='text/css' media='all' />
	<link rel='stylesheet' href='css/fontello/css/fontello.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/animation.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/colors.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/styles.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/vendor/mediaelement/mediaelementplayer.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='js/vendor/mediaelement/mediaelement.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
	<link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />

	<link rel="icon" href="images/cropped-fav-big-32x32.jpg" sizes="32x32" />
	<link rel="icon" href="images/cropped-fav-big-192x192.jpg" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="images/cropped-fav-big-180x180.jpg" />
	<meta name="msapplication-TileImage" content="images/cropped-fav-big-270x270.jpg" />

</head>

<body class="page team body_style_wide scheme_default blog_mode_page sidebar_hide header_style_header-2 header_title_on no_layout vc_responsive">
<div class="body_wrap">
	<div class="page_wrap">
		<header class="top_panel top_panel_style_2 scheme_default">
			<div class="top_panel_fixed_wrap"></div>
			<div class="top_panel_navi scheme_dark">
				<div class="menu_main_wrap clearfix">

					<div class="content_wrap">
						<a class="logo scheme_dark" href="index.php">
							<?php $logo = Logo::getLogo(); ?>
							<img src="admin/<?= $logo['img']; ?>" class="logo_main" alt="">
						</a>
						<nav class="menu_main_nav_area menu_hover_fade">
							<div class="menu_main_inner">
								<div class="contact_wrap scheme_default ">
									<div class="phone_wrap icon-mobile"><?php echo $addressInfo['mobile']?></div>
									<div class="socials_wrap">
										<?php $link = $social->getAllSocial(); if (isset($link)){ ?>
											<span class="social_item">
                                            <a href="<?= $link['facebook'] ?>" target="_blank" class="social_icons
                                            social_facebook">
                                                <span class="trx_addons_icon-facebook"></span>
                                            </a>
                                        </span>
											<span class="social_item">
                                            <a href="<?= $link['twitter'] ?>" target="_blank" class="social_icons social_twitter">
                                                <span class="trx_addons_icon-twitter"></span>
                                            </a>
                                        </span>
											<span class="social_item">
                                            <a href="<?= $link['youtube'] ?>" target="_blank" class="social_icons social_youtube">
                                                <span class="trx_addons_icon-youtube"></span>
                                            </a>
                                        </span>
										<?php } ?>
									</div>
									<div class="search_wrap search_style_fullscreen">
										<div class="search_form_wrap">
											<form role="search" method="get" class="search_form" action="#">
												<input type="text" class="search_field" placeholder="Search" value="" name="s">
												<button type="submit" class="search_submit icon-search"></button>
												<a class="search_close icon-cancel"></a>
											</form>
										</div>
										<div class="search_results widget_area">
											<a href="#" class="search_results_close icon-cancel"></a>
											<div class="search_results_content"></div>
										</div>
									</div>
								</div>
								<ul id="menu_main" class="sc_layouts_menu_nav menu_main_nav">
									<?php
									$allMenu = Menu::getAllMenu();
									if (isset($allMenu)){
										$x = 0;
										foreach ($allMenu as $menu) {
											$x++;
											$allSubMenu = SubMenu::getMenuById($menu['id']);
											?>
											<li class="menu-item <?php echo !empty($allSubMenu) ? 'menu-item-has-children':NULL ?>
                                            <?php
											if (strtolower($menu['name']) == 'home'){echo 'current-menu-ancestor';} ?>">
												<a href="main_details.php?pageId=<?= urlencode($menu['id']); ?>"><span>
                                                        <?= $menu['name']; ?></span></a>
												<?php
												if (!empty($allSubMenu)){
													?>
													<ul class="sub-menu">
														<?php
														foreach ($allSubMenu as $sub) {
															$allChildMenu = SubMenu::getChildById($sub['id']);
															?>
															<li class="menu-item <?php echo !empty($allChildMenu) ? 'menu-item-has-children':NULL ?>">
																<a href="sub_details.php?subPageId=<?= urlencode($sub['id']); ?>">
                                                                    <span>
                                                                        <?= $sub['name']; ?>
                                                                    </span>
																</a>
																<?php

																if (!empty($allChildMenu)) {
																	?>
																	<ul class="sub-menu">
																		<?php
																		foreach ($allChildMenu as $child) {
																			?>
																			<li class="menu-item">
																				<a href="chaild_details.php?childId=<?= urlencode($child['id']); ?>"><span><?= $child['name']; ?></span></a>
																			</li>
																		<?php } ?>
																	</ul>
																<?php } ?>
															</li>
														<?php }
														if ($x <= 1){
															?>
															<li class="menu-item">
																<a href="team.php">
																	<span>Our Team</span>
																</a>
															</li>
														<?php } if ($x == 2){?>
															<li class="menu-item">
																<a href="portfolio.php">
																	<span>Our Activities</span>
																</a>
															</li>
														<?php } ?>
													</ul>
												<?php } ?>
											</li>
											<?php
										}}
									?>
								</ul>
							</div>
						</nav>
						<a class="menu_mobile_button"></a>
					</div>
				</div>
			</div>
			<div class="top_panel_title_wrap">
				<div class="content_wrap">
					<div class="top_panel_title">
						<div class="page_title">
							<h3 class="page_caption">Our Activities</h3>
						</div>
						<div class="breadcrumbs">
							<a class="breadcrumbs_item home" href="index.php">Home</a>
							<span class="breadcrumbs_delimiter"></span>
							<span class="breadcrumbs_item current">Our Activities</span>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="menu_mobile_overlay"></div>
		<div class="menu_mobile scheme_">
			<div class="menu_mobile_inner">
				<a class="menu_mobile_close icon-cancel"></a>

				<nav class="menu_mobile_nav_area">
					<ul id="menu_mobile" class="sc_layouts_menu_nav menu_main_nav">
						<?php
						$allMenu = Menu::getAllMenu();
						if (isset($allMenu)){
							$x = 0;
							foreach ($allMenu as $menu) {
								$x++;
								$allSubMenu = SubMenu::getMenuById($menu['id']);
								?>
								<li class="menu-item <?php echo !empty($allSubMenu) ? 'menu-item-has-children':NULL ?>
                                <?php
								if (strtolower($menu['name']) == 'home'){echo 'current-menu-ancestor';} ?>">
									<a href="main_details.php?pageId=<?= urlencode($menu['id']); ?>">
										<span><?= $menu['name']; ?></span></a>
									<?php
									if (!empty($allSubMenu)){
										?>
										<ul class="sub-menu">
											<?php
											foreach ($allSubMenu as $sub) {
												$allChildMenu = SubMenu::getChildById($sub['id']);
												?>
												<li class="menu-item <?php echo !empty($allChildMenu) ? 'menu-item-has-children':NULL ?>">
													<a href="sub_details.php?subPageId=<?= urlencode($sub['id']); ?>">
                                                        <span><?= $sub['name'];
	                                                        ?></span></a>
													<?php

													if (!empty($allChildMenu)) {
														?>
														<ul class="sub-menu">
															<?php
															foreach ($allChildMenu as $child) {
																?>
																<li class="menu-item">
																	<a href="child_details.php?childId=<?= urlencode($child['id']); ?>">
																		<span><?= $child['name']; ?></span></a>
																</li>
															<?php } ?>
														</ul>
													<?php } ?>
												</li>
											<?php }
											if ($x <= 1){
												?>
												<li class="menu-item">
													<a href="team.php">
														<span>Our Team</span>
													</a>
												</li>
											<?php } if ($x  == 2){?>
												<li class="menu-item">
													<a href="portfolio.php">
														<span>Our Activities</span>
													</a>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
								</li>
								<?php
							}}
						?>
					</ul>
				</nav>
				<div class="search_mobile">
					<div class="search_form_wrap">
						<form role="search" method="get" class="search_form" action="#">
							<input type="text" class="search_field" placeholder="Search ..." value="" name="s">
							<button type="submit" class="search_submit icon-search" title="Start search"></button>
						</form>
					</div>
				</div>
				<div class="socials_mobile">
                    <span class="social_item">
                        <a href="<?= $link['facebook'] ?>" target="_blank" class="social_icons social_facebook">
                            <span class="trx_addons_icon-facebook"></span>
                        </a>
                    </span>
					<span class="social_item">
                        <a href="<?= $link['twitter'] ?>" target="_blank" class="social_icons social_twitter">
                            <span class="trx_addons_icon-twitter"></span>
                        </a>
                    </span>
					<span class="social_item">
                        <a href="<?= $link['youtube'] ?>" target="_blank" class="social_icons social_yutube">
                            <span class="trx_addons_icon-gplus"></span>
                        </a>
                    </span>
				</div>
			</div>
		</div>
		<div class="page_content_wrap scheme_default">
			<div class="content_wrap">
				<div class="content">
					<article class="post_item_single page hentry">
						<div class="post_content entry-content">
							<div class="vc_row wpb_row vc_row-fluid fullwidth_1">

							</div>
							<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1468167409309 vc_row-has-fill fullwidth_1">
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner ">
										<div class="wpb_wrapper">
											<div class="sc_team sc_team_default" data-slides-per-view="3" data-slides-min-width="150">
												<div class="sc_team_columns sc_item_columns trx_addons_columns_wrap columns_padding_bottom">
													<?php
                                                    $portfolioInfo = Portfolio::getAllPortfolio();
                                                    if(isset($portfolioInfo)){
                                                        foreach ($portfolioInfo as $info){
                                                    ?>
													<div class="trx_addons_column-1_3">
														<div class="sc_team_item">
															<div class="sc_team_item_avatar trx_addons_hover trx_addons_hover_style_zoomin">
																<img src="admin/<?= $info['location']; ?>" alt="" />
																<div class="trx_addons_hover_mask"></div>
																<div class="trx_addons_hover_content">
																	<h5 class="trx_addons_hover_title">
																		<a href="single-portfolio.php?portId=<?=
                                                                        $info['id']; ?>">
                                                                            <?= $info['name']; ?>
                                                                        </a>
																	</h5>
																	<a href="single-portfolio.php?portId=<?= $info['id']; ?>"
                                                                       class="trx_addons_hover_icon trx_addons_hover_icon_link"></a>
																	<a href="admin/<?= $info['location']; ?>"
                                                                       class="trx_addons_hover_icon trx_addons_hover_icon_zoom"></a>
																</div>
															</div>
														</div>
													</div>
                                                    <?php }} ?>
												</div>

											</div>
											<div class="vc_empty_space empty_6">
												<span class="vc_empty_space_inner"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="vc_row-full-width vc_clearfix"></div>
							<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid">

							</div>
							<div class="vc_row-full-width vc_clearfix"></div>
						</div>
					</article>

				</div>
			</div>
		</div>


		<footer class="site_footer_wrap">
			<div class="footer_wrap widget_area scheme_dark">
				<div class="footer_wrap_inner widget_area_inner">
					<div class="content_wrap">

						<div class="columns_wrap">
							<aside class="column-1_4 widget widget_text">
								<div class="textwidget">
									<img src="admin/<?= $logo['img']; ?>" alt="HR Advisor">
									<ul class="trx_addons_list  icons">
										<li class="icon-home-alt">
											<?php echo wordwrap(ucwords($addressInfo['location']),28,"<br>\n"); ?>
										</li>
										<li class="icon-white"><?php echo $addressInfo['email']?></li>
										<li class="icon-tablet"><?php echo $addressInfo['mobile']?></li>
									</ul>
								</div>
							</aside>
							<?php
							if (isset($allMenu)){
								$x = 0;
								foreach ($allMenu as $menu) {
									$x ++;
									if ($x <= 3){
										$allSubMenu = SubMenu::getMenuById($menu['id']);
										?>
										<aside class="column-1_4 widget widget_nav_menu">
											<h5 class="widget_title"><?= $menu['name']; ?></h5>
											<div class="menu-footer-links-container">
												<ul id="menu-footer-links" class="menu">
													<?php
													if (!empty($allSubMenu)){
														foreach ($allSubMenu as $sub) {
															?>
															<li class="menu-item"><a href="sub_details.php?id=<?= urlencode($sub['id']); ?>"><?= $sub['name']; ?></a></li>
														<?php }} ?>
                                                    <?php
                                                    if ($x <= 1){
                                                    ?>
                                                    <li class="menu-item">
                                                        <a href="team.php">
                                                            <span>Our Team</span>
                                                        </a>
                                                    </li>
                                                <?php } if ($x == 2){?>
                                                    <li class="menu-item">
                                                        <a href="portfolio.php">
                                                            <span>Our Activities</span>
                                                        </a>
                                                    </li>
                                                <?php } ?>
												</ul>
											</div>
										</aside>
									<?php }}} ?>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright_wrap scheme_dark">
				<div class="copyright_wrap_inner">
					<div class="content_wrap">
						<div class="copyright_text">
							<div class="columns_wrap">
								<?php
								$info = CopyRight::getText();
								?>
								<div class="column-1_2"><?= $info['title']; ?> &copy;<?php echo date('Y'); ?>. All rights
									reserved
									.</div>
								<div class="column-1_2">
									<a href="#">Terms of use</a> and
									<a href="#">Privacy Policy</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>

<script type='text/javascript' src='js/vendor/jQuery/jquery.js'></script>
<script type='text/javascript' src='js/vendor/jQuery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='js/custom/custom.js'></script>
<script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>
<script type='text/javascript' src='js/custom/trx/trx_addons.js'></script>
<script type='text/javascript' src='js/custom/scripts.js'></script>
<script type='text/javascript' src='js/vendor/mediaelement/mediaelement-and-player.min.js'></script>
<script type='text/javascript' src='js/vendor/mediaelement/mediaelement.min.js'></script>
<script type='text/javascript' src='js/custom/embed.min.js'></script>
<script type='text/javascript' src='js/vendor/js_comp/js_comp.min.js'></script>
<script type='text/javascript' src='js/vendor/js_comp/waypoints.min.js'></script>
<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>

</html>
<?php ob_end_flush(); ?>